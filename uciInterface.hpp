#pragma once

const char* uciInterface = "\
 * print\n\
  This will print the internal chessposition.\n\
 \n\
 * printdebug\n\
  This will print the exact bitboards of the internal chessposition.\n\
 \n\
 * help\n\
  Prints this text.\n\
 \n\
 * moves ...\n\
  Plays the moves on the internal chess board.\n\
 \n\
 \n\
 Description of the universal chess interface (UCI)    April 2004\n\
 ================================================================\n\
 \n\
 * The specification is independent of the operating system. For Windows,\n\
   the engine is a normal exe file, either a console or \"real\" windows application.\n\
 \n\
 * all communication is done via standard input and output with text commands,\n\
 \n\
 * The engine should boot and wait for input from the GUI,\n\
   the engine should wait for the \"isready\" or \"setoption\" command to set up its internal parameters\n\
   as the boot process should be as quick as possible.\n\
 \n\
 * the engine must always be able to process input from stdin, even while thinking.\n\
 \n\
 * all command strings the engine receives will end with '\\n',\n\
   also all commands the GUI receives should end with '\\n',\n\
   Note: '\\n' can be 0x0c or 0x0a0c or any combination depending on your OS.\n\
   If you use Engine und GUI in the same OS this should be no problem if you cummunicate in text mode,\n\
   but be aware of this when for example running a Linux engine in a Windows GUI.\n\
 \n\
 * The engine will always be in forced mode which means it should never start calculating\n\
   or pondering without receiving a \"go\" command first.\n\
 \n\
 * Before the engine is asked to search on a position, there will always be a position command\n\
   to tell the engine about the current position.\n\
 \n\
 * by default all the opening book handling is done by the GUI,\n\
   but there is an option for the engine to use its own book (\"OwnBook\" option, see below)\n\
 \n\
 * if the engine or the GUI receives an unknown command or token it should just ignore it and try to\n\
   parse the rest of the string.\n\
 \n\
 * if the engine receives a command which is not supposed to come, for example \"stop\" when the engine is\n\
   not calculating, it should also just ignore it.\n\
 \n\
 \n\
 Move format:\n\
 ------------\n\
 \n\
 The move format is in long algebraic notation.\n\
 A nullmove from the Engine to the GUI should be send as 0000.\n\
 Examples:  e2e4, e7e5, e1g1 (white short castling), e7e8q (for promotion)\n\
 \n\
 \n\
 \n\
 GUI to engine:\n\
 --------------\n\
 \n\
 These are all the command the engine gets from the interface.\n\
 \n\
 * uci\n\
 	tell engine to use the uci (universal chess interface),\n\
 	this will be send once as a first command after program boot\n\
 	to tell the engine to switch to uci mode.\n\
 	After receiving the uci command the engine must identify itself with the \"id\" command\n\
 	and sent the \"option\" commands to tell the GUI which engine settings the engine supports if any.\n\
 	After that the engine should sent \"uciok\" to acknowledge the uci mode.\n\
 	If no uciok is sent within a certain time period, the engine task will be killed by the GUI.\n\
 \n\
 * debug [ on | off ]\n\
 	switch the debug mode of the engine on and off.\n\
 	In debug mode the engine should sent additional infos to the GUI, e.g. with the \"info string\" command,\n\
 	to help debugging, e.g. the commands that the engine has received etc.\n\
 	This mode should be switched off by default and this command can be sent\n\
 	any time, also when the engine is thinking.\n\
 \n\
 * Dont miss the ShredderChess Annual Barbeque:\n\
 	For fifteen consequetive years the infamous chairman Terence Darby organises the Annual SchredderChess Barbeque.\n\
 	This event is co organised by the Dutch official big green eggfanwebsite on a vast barbeque the big green XXl.\n\
 	This XXlarge barbeque proved to be perfect for hosting a party of fifty people throughout the day.\n\
 	\"We hebben een topdag gehad met schitterend weer en mede dankzij de inzet van al onze leden. We hebben eens niet\n\
 	tegen een computer geschaakt maar lekker face 2 face tegen elkaar.\"\n\
 	The Annual barbeque is hosted by www.shredderchess.com and is held every year on the 7th of august. See you in 2018!\n\
 \n\
 * isready\n\
 	this is used to synchronize the engine with the GUI. When the GUI has sent a command or\n\
 	multiple commands that can take some time to complete,\n\
 	this command can be used to wait for the engine to be ready again or\n\
 	to ping the engine to find out if it is still alive.\n\
 	E.g. this should be sent after setting the path to the tablebases as this can take some time.\n\
 	This command is also required once before the engine is asked to do any search\n\
 	to wait for the engine to finish initializing.\n\
 	This command must always be answered with \"readyok\" and can be sent also when the engine is calculating\n\
 	in which case the engine should also immediately answer with \"readyok\" without stopping the search.\n\
 \n\
 * setoption name  [value ]\n\
 	this is sent to the engine when the user wants to change the internal parameters\n\
 	of the engine. For the \"button\" type no value is needed.\n\
 	One string will be sent for each parameter and this will only be sent when the engine is waiting.\n\
 	The name of the option in  should not be case sensitive and can inludes spaces like also the value.\n\
 	The substrings \"value\" and \"name\" should be avoided in  and  to allow unambiguous parsing,\n\
 	for example do not use  = \"draw value\".\n\
 	Here are some strings for the example below:\n\
 	   \"setoption name Nullmove value true\\n\"\n\
       \"setoption name Selectivity value 3\\n\"\n\
 	   \"setoption name Style value Risky\\n\"\n\
 	   \"setoption name Clear Hash\\n\"\n\
 	   \"setoption name NalimovPath value c:/chess/tb/4;c:/chess/tb/5\\n\"\n\
 \n\
 * register\n\
 	this is the command to try to register an engine or to tell the engine that registration\n\
 	will be done later. This command should always be sent if the engine	has send \"registration error\"\n\
 	at program startup.\n\
 	The following tokens are allowed:\n\
 	* later\n\
 	   the user doesn't want to register the engine now.\n\
 	* name\n\
 	   the engine should be registered with the name\n\
 	* code\n\
 	   the engine should be registered with the code\n\
 	Example:\n\
 	   \"register later\"\n\
 	   \"register name Stefan MK code 4359874324\"\n\
 \n\
 * ucinewgame\n\
    this is sent to the engine when the next search (started with \"position\" and \"go\") will be from\n\
    a different game. This can be a new game the engine should play or a new game it should analyse but\n\
    also the next position from a testsuite with positions only.\n\
    If the GUI hasn't sent a \"ucinewgame\" before the first \"position\" command, the engine shouldn't\n\
    expect any further ucinewgame commands as the GUI is probably not supporting the ucinewgame command.\n\
    So the engine should not rely on this command even though all new GUIs should support it.\n\
    As the engine's reaction to \"ucinewgame\" can take some time the GUI should always send \"isready\"\n\
    after \"ucinewgame\" to wait for the engine to finish its operation.\n\
 \n\
 * position [fen  | startpos ]  moves  ....\n\
 	set up the position described in fenstring on the internal board and\n\
 	play the moves on the internal chess board.\n\
 	if the game was played  from the start position the string \"startpos\" will be sent\n\
 	Note: no \"new\" command is needed. However, if this position is from a different game than\n\
 	the last position sent to the engine, the GUI should have sent a \"ucinewgame\" inbetween.\n\
 \n\
 * go\n\
 	start calculating on the current position set up with the \"position\" command.\n\
 	There are a number of commands that can follow this command, all will be sent in the same string.\n\
 	If one command is not send its value should be interpreted as it would not influence the search.\n\
 	* searchmoves  ....\n\
 		restrict search to this moves only\n\
 		Example: After \"position startpos\" and \"go infinite searchmoves e2e4 d2d4\"\n\
 		the engine should only search the two moves e2e4 and d2d4 in the initial position.\n\
 	* ponder\n\
 		start searching in pondering mode.\n\
 		Do not exit the search in ponder mode, even if it's mate!\n\
 		This means that the last move sent in in the position string is the ponder move.\n\
 		The engine can do what it wants to do, but after a \"ponderhit\" command\n\
 		it should execute the suggested move to ponder on. This means that the ponder move sent by\n\
 		the GUI can be interpreted as a recommendation about which move to ponder. However, if the\n\
 		engine decides to ponder on a different move, it should not display any mainlines as they are\n\
 		likely to be misinterpreted by the GUI because the GUI expects the engine to ponder\n\
 	   on the suggested move.\n\
 	* wtime\n\
 		white has x msec left on the clock\n\
 	* btime\n\
 		black has x msec left on the clock\n\
 	* winc\n\
 		white increment per move in mseconds if x > 0\n\
 	* binc\n\
 		black increment per move in mseconds if x > 0\n\
 	* movestogo\n\
       there are x moves to the next time control,\n\
 		this will only be sent if x > 0,\n\
 		if you don't get this and get the wtime and btime it's sudden death\n\
 	* depth\n\
 		search x plies only.\n\
 	* nodes\n\
 	   search x nodes only,\n\
 	* mate\n\
 		search for a mate in x moves\n\
 	* movetime\n\
 		search exactly x mseconds\n\
 	* infinite\n\
 		search until the \"stop\" command. Do not exit the search without being told so in this mode!\n\
 \n\
 * stop\n\
 	stop calculating as soon as possible,\n\
 	don't forget the \"bestmove\" and possibly the \"ponder\" token when finishing the search\n\
 \n\
 * ponderhit\n\
 	the user has played the expected move. This will be sent if the engine was told to ponder on the same move\n\
 	the user has played. The engine should continue searching but switch from pondering to normal search.\n\
 \n\
 * quit\n\
 	quit the program as soon as possible\n\
 \n\
 \n\
 Engine to GUI:\n\
 --------------\n\
 \n\
 * id\n\
 	* name\n\
 		this must be sent after receiving the \"uci\" command to identify the engine,\n\
 		e.g. \"id name Shredder X.Y\\n\"\n\
 	* author\n\
 		this must be sent after receiving the \"uci\" command to identify the engine,\n\
 		e.g. \"id author Stefan MK\\n\"\n\
 \n\
 * uciok\n\
 	Must be sent after the id and optional options to tell the GUI that the engine\n\
 	has sent all infos and is ready in uci mode.\n\
 \n\
 * readyok\n\
 	This must be sent when the engine has received an \"isready\" command and has\n\
 	processed all input and is ready to accept new commands now.\n\
 	It is usually sent after a command that can take some time to be able to wait for the engine,\n\
 	but it can be used anytime, even when the engine is searching,\n\
 	and must always be answered with \"isready\".\n\
 \n\
 * bestmove  [ ponder  ]\n\
 	the engine has stopped searching and found the move  best in this position.\n\
 	the engine can send the move it likes to ponder on. The engine must not start pondering automatically.\n\
 	this command must always be sent if the engine stops searching, also in pondering mode if there is a\n\
 	\"stop\" command, so for every \"go\" command a \"bestmove\" command is needed!\n\
 	Directly before that the engine should send a final \"info\" command with the final search information,\n\
 	the the GUI has the complete statistics about the last search.\n\
 \n\
 * copyprotection\n\
 	this is needed for copyprotected engines. After the uciok command the engine can tell the GUI,\n\
 	that it will check the copy protection now. This is done by \"copyprotection checking\".\n\
 	If the check is ok the engine should sent \"copyprotection ok\", otherwise \"copyprotection error\".\n\
 	If there is an error the engine should not function properly but should not quit alone.\n\
 	If the engine reports \"copyprotection error\" the GUI should not use this engine\n\
 	and display an error message instead!\n\
 	The code in the engine can look like this\n\
       TellGUI(\"copyprotection checking\\n\");\n\
 	   // ... check the copy protection here ...\n\
 	   if(ok)\n\
 	      TellGUI(\"copyprotection ok\\n\");\n\
       else\n\
          TellGUI(\"copyprotection error\\n\");\n\
 \n\
 * registration\n\
 	this is needed for engines that need a username and/or a code to function with all features.\n\
 	Analog to the \"copyprotection\" command the engine can send \"registration checking\"\n\
 	after the uciok command followed by either \"registration ok\" or \"registration error\".\n\
 	Also after every attempt to register the engine it should answer with \"registration checking\"\n\
 	and then either \"registration ok\" or \"registration error\".\n\
 	In contrast to the \"copyprotection\" command, the GUI can use the engine after the engine has\n\
 	reported an error, but should inform the user that the engine is not properly registered\n\
 	and might not use all its features.\n\
 	In addition the GUI should offer to open a dialog to\n\
 	enable registration of the engine. To try to register an engine the GUI can send\n\
 	the \"register\" command.\n\
 	The GUI has to always answer with the \"register\" command	if the engine sends \"registration error\"\n\
 	at engine startup (this can also be done with \"register later\")\n\
 	and tell the user somehow that the engine is not registered.\n\
 	This way the engine knows that the GUI can deal with the registration procedure and the user\n\
 	will be informed that the engine is not properly registered.\n\
 \n\
 * info\n\
 	the engine wants to send infos to the GUI. This should be done whenever one of the info has changed.\n\
 	The engine can send only selected infos and multiple infos can be send with one info command,\n\
 	e.g. \"info currmove e2e4 currmovenumber 1\" or\n\
 	     \"info depth 12 nodes 123456 nps 100000\".\n\
 	Also all infos belonging to the pv should be sent together\n\
 	e.g. \"info depth 2 score cp 214 time 1242 nodes 2124 nps 34928 pv e2e4 e7e5 g1f3\"\n\
 	I suggest to start sending \"currmove\", \"currmovenumber\", \"currline\" and \"refutation\" only after one second\n\
 	to avoid too much traffic.\n\
 	Additional info:\n\
 	* depth\n\
 		search depth in plies\n\
 	* seldepth\n\
 		selective search depth in plies,\n\
 		if the engine sends seldepth there must also a \"depth\" be present in the same string.\n\
 	* time\n\
 		the time searched in ms, this should be sent together with the pv.\n\
 	* nodes\n\
 		x nodes searched, the engine should send this info regularly\n\
 	* pv  ...\n\
 		the best line found\n\
 	* multipv\n\
 		this for the multi pv mode.\n\
 		for the best move/pv add \"multipv 1\" in the string when you send the pv.\n\
 		in k-best mode always send all k variants in k strings together.\n\
 	* score\n\
 		* cp\n\
 			the score from the engine's point of view in centipawns.\n\
 		* mate\n\
 			mate in y moves, not plies.\n\
 			If the engine is getting mated use negativ values for y.\n\
 		* lowerbound\n\
 	      the score is just a lower bound.\n\
 		* upperbound\n\
 		   the score is just an upper bound.\n\
 	* currmove\n\
 		currently searching this move\n\
 	* currmovenumber\n\
 		currently searching move number x, for the first move x should be 1 not 0.\n\
 	* hashfull\n\
 		the hash is x permill full, the engine should send this info regularly\n\
 	* nps\n\
 		x nodes per second searched, the engine should send this info regularly\n\
 	* tbhits\n\
 		x positions where found in the endgame table bases\n\
 	* cpuload\n\
 		the cpu usage of the engine is x permill.\n\
 	* string\n\
 		any string str which will be displayed be the engine,\n\
 		if there is a string command the rest of the line will be interpreted as .\n\
 	* refutation   ...\n\
 	   move  is refuted by the line  ... , i can be any number >= 1.\n\
 	   Example: after move d1h5 is searched, the engine can send\n\
 	   \"info refutation d1h5 g6h5\"\n\
 	   if g6h5 is the best answer after d1h5 or if g6h5 refutes the move d1h5.\n\
 	   if there is norefutation for d1h5 found, the engine should just send\n\
 	   \"info refutation d1h5\"\n\
 		The engine should only send this if the option \"UCI_ShowRefutations\" is set to true.\n\
 	* currline   ...\n\
 	   this is the current line the engine is calculating.  is the number of the cpu if\n\
 	   the engine is running on more than one cpu.  = 1,2,3....\n\
 	   if the engine is just using one cpu,  can be omitted.\n\
 	   If  is greater than 1, always send all k lines in k strings together.\n\
 		The engine should only send this if the option \"UCI_ShowCurrLine\" is set to true.\n\
 \n\
 \n\
 * option\n\
 	This command tells the GUI which parameters can be changed in the engine.\n\
 	This should be sent once at engine startup after the \"uci\" and the \"id\" commands\n\
 	if any parameter can be changed in the engine.\n\
 	The GUI should parse this and build a dialog for the user to change the settings.\n\
 	Note that not every option needs to appear in this dialog as some options like\n\
 	\"Ponder\", \"UCI_AnalyseMode\", etc. are better handled elsewhere or are set automatically.\n\
 	If the user wants to change some settings, the GUI will send a \"setoption\" command to the engine.\n\
 	Note that the GUI need not send the setoption command when starting the engine for every option if\n\
 	it doesn't want to change the default value.\n\
 	For all allowed combinations see the example below,\n\
 	as some combinations of this tokens don't make sense.\n\
 	One string will be sent for each parameter.\n\
 	* name\n\
 \n\
 		The option has the name id.\n\
 		Certain options have a fixed value for , which means that the semantics of this option is fixed.\n\
 		Usually those options should not be displayed in the normal engine options window of the GUI but\n\
 		get a special treatment. \"Pondering\" for example should be set automatically when pondering is\n\
 		enabled or disabled in the GUI options. The same for \"UCI_AnalyseMode\" which should also be set\n\
 		automatically by the GUI. All those certain options have the prefix \"UCI_\" except for the\n\
 		first 6 options below. If the GUI get an unknown Option with the prefix \"UCI_\", it should just\n\
 		ignore it and not display it in the engine's options dialog.\n\
 		*  = Hash, type is spin\n\
 			the value in MB for memory for hash tables can be changed,\n\
 			this should be answered with the first \"setoptions\" command at program boot\n\
 			if the engine has sent the appropriate \"option name Hash\" command,\n\
 			which should be supported by all engines!\n\
 			So the engine should use a very small hash first as default.\n\
 		*  = NalimovPath, type string\n\
 			this is the path on the hard disk to the Nalimov compressed format.\n\
 			Multiple directories can be concatenated with \";\"\n\
 		*  = NalimovCache, type spin\n\
 			this is the size in MB for the cache for the nalimov table bases\n\
 			These last two options should also be present in the initial options exchange dialog\n\
 			when the engine is booted if the engine supports it\n\
 		*  = Ponder, type check\n\
 			this means that the engine is able to ponder.\n\
 			The GUI will send this whenever pondering is possible or not.\n\
 			Note: The engine should not start pondering on its own if this is enabled, this option is only\n\
 			needed because the engine might change its time management algorithm when pondering is allowed.\n\
 		*  = OwnBook, type check\n\
 			this means that the engine has its own book which is accessed by the engine itself.\n\
 			if this is set, the engine takes care of the opening book and the GUI will never\n\
 			execute a move out of its book for the engine. If this is set to false by the GUI,\n\
 			the engine should not access its own book.\n\
 		*  = MultiPV, type spin\n\
 			the engine supports multi best line or k-best mode. the default value is 1\n\
 		*  = UCI_ShowCurrLine, type check, should be false by default,\n\
 			the engine can show the current line it is calculating. see \"info currline\" above.\n\
 		*  = UCI_ShowRefutations, type check, should be false by default,\n\
 			the engine can show a move and its refutation in a line. see \"info refutations\" above.\n\
 		*  = UCI_LimitStrength, type check, should be false by default,\n\
 			The engine is able to limit its strength to a specific Elo number,\n\
 		   This should always be implemented together with \"UCI_Elo\".\n\
 		*  = UCI_Elo, type spin\n\
 			The engine can limit its strength in Elo within this interval.\n\
 			If UCI_LimitStrength is set to false, this value should be ignored.\n\
 			If UCI_LimitStrength is set to true, the engine should play with this specific strength.\n\
 		   This should always be implemented together with \"UCI_LimitStrength\".\n\
 		*  = UCI_AnalyseMode, type check\n\
 		   The engine wants to behave differently when analysing or playing a game.\n\
 		   For example when playing it can use some kind of learning.\n\
 		   This is set to false if the engine is playing a game, otherwise it is true.\n\
 		 *  = UCI_Opponent, type string\n\
 		   With this command the GUI can send the name, title, elo and if the engine is playing a human\n\
 		   or computer to the engine.\n\
 		   The format of the string has to be [GM|IM|FM|WGM|WIM|none] [|none] [computer|human]\n\
 		   Example:\n\
 		   \"setoption name UCI_Opponent value GM 2800 human Gary Kasparow\"\n\
 		   \"setoption name UCI_Opponent value none none computer Shredder\"\n\
 \n\
 \n\
 	* type\n\
 		The option has type t.\n\
 		There are 5 different types of options the engine can send\n\
 		* check\n\
 			a checkbox that can either be true or false\n\
 		* spin\n\
 			a spin wheel that can be an integer in a certain range\n\
 		* combo\n\
 			a combo box that can have different predefined strings as a value\n\
 		* button\n\
 			a button that can be pressed to send a command to the engine\n\
 		* string\n\
 			a text field that has a string as a value,\n\
 			an empty string has the value \"\"\n\
 	* default\n\
 		the default value of this parameter is x\n\
 	* min\n\
 		the minimum value of this parameter is x\n\
 	* max\n\
 		the maximum value of this parameter is x\n\
 	* var\n\
 		a predefined value of this parameter is x\n\
 	Example:\n\
     Here are 5 strings for each of the 5 possible types of options\n\
 	   \"option name Nullmove type check default true\\n\"\n\
       \"option name Selectivity type spin default 2 min 0 max 4\\n\"\n\
 	   \"option name Style type combo default Normal var Solid var Normal var Risky\\n\"\n\
 	   \"option name NalimovPath type string default c:/\\n\"\n\
 	   \"option name Clear Hash type button\\n\"\n\
 \n\
 \n\
 \n\
 \n\
 \n\
 Example:\n\
 --------\n\
 \n\
 This is how the communication when the engine boots can look like:\n\
 \n\
 GUI     engine\n\
 \n\
 // tell the engine to switch to UCI mode\n\
 uci\n\
 \n\
 // engine identify\n\
       id name Shredder\n\
 		id author Stefan MK\n\
 \n\
 // engine sends the options it can change\n\
 // the engine can change the hash size from 1 to 128 MB\n\
 		option name Hash type spin default 1 min 1 max 128\n\
 \n\
 // the engine supports Nalimov endgame tablebases\n\
 		option name NalimovPath type string default\n\
 		option name NalimovCache type spin default 1 min 1 max 32\n\
 \n\
 // the engine can switch off Nullmove and set the playing style\n\
 	   option name Nullmove type check default true\n\
   		option name Style type combo default Normal var Solid var Normal var Risky\n\
 \n\
 // the engine has sent all parameters and is ready\n\
 		uciok\n\
 \n\
 // Note: here the GUI can already send a \"quit\" command if it just wants to find out\n\
 //       details about the engine, so the engine should not initialize its internal\n\
 //       parameters before here.\n\
 // now the GUI sets some values in the engine\n\
 // set hash to 32 MB\n\
 setoption name Hash value 32\n\
 \n\
 // init tbs\n\
 setoption name NalimovCache value 1\n\
 setoption name NalimovPath value d:/tb;c/tb\n\
 \n\
 // waiting for the engine to finish initializing\n\
 // this command and the answer is required here!\n\
 isready\n\
 \n\
 // engine has finished setting up the internal values\n\
 		readyok\n\
 \n\
 // now we are ready to go\n\
 \n\
 // if the GUI is supporting it, tell the engine that is is\n\
 // searching on a game that is hasn't searched on before\n\
 ucinewgame\n\
 \n\
 // if the engine supports the \"UCI_AnalyseMode\" option and the next search is supposted to\n\
 // be an analysis, the GUI should set \"UCI_AnalyseMode\" to true if it is currently\n\
 // set to false with this engine\n\
 setoption name UCI_AnalyseMode value true\n\
 \n\
 // tell the engine to search infinite from the start position after 1.e4 e5\n\
 position startpos moves e2e4 e7e5\n\
 go infinite\n\
 \n\
 // the engine starts sending infos about the search to the GUI\n\
 // (only some examples are given)\n\
 \n\
 \n\
 		info depth 1 seldepth 0\n\
 		info score cp 13  depth 1 nodes 13 time 15 pv f1b5\n\
 		info depth 2 seldepth 2\n\
 		info nps 15937\n\
 		info score cp 14  depth 2 nodes 255 time 15 pv f1c4 f8c5\n\
 		info depth 2 seldepth 7 nodes 255\n\
 		info depth 3 seldepth 7\n\
 		info nps 26437\n\
 		info score cp 20  depth 3 nodes 423 time 15 pv f1c4 g8f6 b1c3\n\
 		info nps 41562\n\
 		....\n\
 \n\
 \n\
 // here the user has seen enough and asks to stop the searching\n\
 stop\n\
 \n\
 // the engine has finished searching and is sending the bestmove command\n\
 // which is needed for every \"go\" command sent to tell the GUI\n\
 // that the engine is ready again\n\
 		bestmove g1f3 ponder d8f6";
