#pragma once

#include <cstdint>
#include <cstddef>

namespace BitOperations
{
  inline int trailingZeros(const uint64_t mask);
  inline int leadingZeros(const uint64_t mask);
  inline int countOnes(const uint64_t mask);

  inline size_t findAndClearTrailingOne(uint64_t & mask)
  {
    size_t ret = trailingZeros(mask);
    mask &= mask -1;
    return ret;
  }

  #if defined(__GNUC__) || defined(__clang__)
    inline int trailingZeros(const uint64_t mask)
    {
      return __builtin_ctzll(mask);
    }
    inline int leadingZeros(const uint64_t mask)
    {
      return __builtin_clzll(mask);
    }
    inline int countOnes(const uint64_t mask)
    {
      return __builtin_popcountll(mask);
    }
  #endif
}
