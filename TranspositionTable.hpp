#pragma once

#include "ChessTypes.hpp"
#include "Move.hpp"

#include <vector>
#include <bitset>
#include <iostream>

// TODO: move to cpp file

using namespace ChessTypes;

struct TranspositionTableEntry
{
  uint64_t zobristKey;
  Value value;
  Ply depth;
  NodeType nodeType;
  bool failedHigh;
  Square bestMoveFrom;
  Square bestMoveTo;
};

class TranspositionTable
{
  std::vector<TranspositionTableEntry> tt;

  int power;
  uint64_t mask;

  inline static uint64_t counter = 0;

  size_t getIndex(uint64_t zobristKey) const
  {
    size_t index = zobristKey & mask;
    for(int i = 0; i<4; i++)
    {
      if(tt[index].zobristKey != 0 && tt[index].zobristKey != zobristKey)
      {
        index = (index+1) & mask;
      }
      else
      {
        break;
      }
    }
    return index;
  }

public:

  TranspositionTable(size_t size)
  {
    size = size/sizeof(TranspositionTableEntry);
    mask = 0;
    for(power = 0; size >= (1U << (power+1)); power++)
    {
      mask |= 1U << power;
    }
    tt = std::vector<TranspositionTableEntry>(1U << power);
  }

  void clear()
  {
    counter = 0;
    for(size_t i = 0; i<tt.size(); i++)
    {
      tt[i].zobristKey = 0;
      tt[i].depth = 0;
    }
  }

  size_t size()
  {
    return tt.size()*sizeof(TranspositionTableEntry);
  }

  size_t hashFull()
  {
    size_t counter = 1;
    for(auto i : tt)
    {
      if(i.zobristKey != 0)
      {
        counter += 1;
      }
    }
    return counter*1000/tt.size();
  }

  void add(uint64_t zobristKey, Value value, Ply depth, NodeType nodeType, const Move& bestMove)
  {
    const size_t index = getIndex(zobristKey);
    if(tt[index].depth <= depth)
    {
      new (&tt[index]) TranspositionTableEntry{zobristKey, value, depth, nodeType, false, bestMove.from, bestMove.to};
    }
  }

  auto getValue(uint64_t zobristKey, Ply minDepth) const
  {
    struct Ret{ Value value; NodeType nodeType; Square bestMoveFrom; Square bestMoveTo; };
    const size_t index = getIndex(zobristKey);
    if(tt[index].zobristKey == zobristKey && tt[index].depth >= minDepth)
    {
      return Ret{tt[index].value, tt[index].nodeType, tt[index].bestMoveFrom, tt[index].bestMoveTo};
    }
    return Ret{NO_VALUE, NO_NODE, NO_SQUARE, NO_SQUARE};
  }

  NodeType getNodeType(uint64_t zobristKey) const
  {
    const size_t index = getIndex(zobristKey);
    if(tt[index].zobristKey == zobristKey)
    {
      return tt[index].nodeType;
    }
    return NO_NODE;
  }

  void setFailedHigh(uint64_t zobristKey)
  {
    const size_t index = getIndex(zobristKey);
    if(tt[index].zobristKey == zobristKey)
    {
      tt[index].failedHigh = true;
    }
    else
    {
      add(zobristKey, NO_VALUE, 0, CUT_NODE, NO_MOVE);
      const size_t newIndex = getIndex(zobristKey);
      if(tt[newIndex].zobristKey == zobristKey)
      {
        tt[newIndex].failedHigh = true;
      }
    }
  }

  bool failedHigh(uint64_t zobristKey) const
  {
    const size_t index = getIndex(zobristKey);
    if(tt[index].zobristKey == zobristKey)
    {
      return tt[index].failedHigh;
    }
    return false;
  }
};
