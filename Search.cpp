#include "Search.hpp"
#include "ChessTypes.hpp"
#include "BitOperations.hpp"
#include "Util.hpp"

#include <string>
#include <cstring>
#include <algorithm>
#include <thread>

using namespace BitOperations;

constexpr size_t NUM_KILLER_MOVES = 2;

constexpr Ply MAX_HEIGHT_HISTORY_HEURISTIC = 10;

constexpr Value DELTA_MARGIN = 150;
constexpr Value FUTILITY_MARGIN = 200;

constexpr Ply NULL_MOVE_DEPTH_REDUCTION = 2;
constexpr int8_t NULL_MOVE_MIN_NUMBER_PIECES_EXCEPT_PAWN_AND_KING = 2;

constexpr int LMR_MOVE_INDEX = 4;

namespace
{
  class PvTable
  {
    Move table[MAX_DEPTH+1][MAX_DEPTH+1];
  public:

    PvTable()
    {
      std::fill_n(&table[0][0], (MAX_DEPTH+1)*(MAX_DEPTH+1), NO_MOVE);
    }

    void clear()
    {
      for(size_t i = 0; i<MAX_DEPTH+1; i++)
      {
        for(size_t j = 0; j<MAX_DEPTH+1; j++)
        {
          table[i][j].to = NO_MOVE.to;
        }
      }
    }
    void clear(Ply height)
    {
      for(size_t i = height; i<MAX_DEPTH+1; i++)
      {
        table[height][i].to = NO_MOVE.to;
      }
    }
    void update(const Move& move, Ply height)
    {
      table[height][height] = move;
      std::memcpy(&table[height][height+1], &table[height+1][height+1], sizeof(Move)*(MAX_DEPTH-height));
      for(size_t i = height+1; i<MAX_DEPTH+1; i++)
      {
        table[height+1][i].to = NO_MOVE.to;
      }
    }
    std::string getPvString()
    {
      std::string ret = "";
      for(size_t i = 0; table[0][i].to != NO_MOVE.to; i++)
      {
        ret += table[0][i].getNotation() + " ";
      }
      return ret;
    }
    const Move& getPvMove(Ply height)
    {
      if(table[0][height].to == NO_MOVE.to)
      {
        return NO_MOVE;
      }
      return table[0][height];
    }
    bool isPv(uint64_t zobristKey, Ply height)
    {
      return zobristKey == table[0][height].zobristKey;
    }
    std::vector<Move> getPvMoves()
    {
      std::vector<Move> ret = std::vector<Move>();
      for(size_t i = 0; table[0][i].to != NO_MOVE.to; i++)
      {
        ret.push_back(table[0][i]);
      }
      return ret;
    }

    void print()
    {
      for(size_t i = 0; i<10; i++)
      {
        for(size_t j = 0; j<10; j++)
        {
          if(i<j)
          {
            std::cout << "     ";
          }
          else
          {
            std::cout << table[j][i].getNotation();
            if(table[j][i].promoted == NO_PIECE)
            {
              std::cout << " ";
            }
          }
        }
        std::cout << std::endl;
      }
    }
  };

  class KillerMoveTable
  {
    Square killerMoves[MAX_DEPTH][NUM_KILLER_MOVES][2];
  public:

    KillerMoveTable()
    {
      std::fill_n(&killerMoves[0][0][0], MAX_DEPTH*NUM_KILLER_MOVES*2, NO_SQUARE);
    }

    void update(const Move& move, Ply height)
    {
      if(not move.isTactical())
      {
        for(ssize_t i = NUM_KILLER_MOVES-1; i >= 0; i--)
        {
          if(i>=1)
          {
            killerMoves[height][i][1] = killerMoves[height][i-1][1];
            killerMoves[height][i][0] = killerMoves[height][i-1][0];
          }
          else
          {
            killerMoves[height][0][0] = move.from;
            killerMoves[height][0][1] = move.to;
          }
        }
      }
    }
    bool isKillerMove(const Move& move, Ply height) const
    {
      for(ssize_t i = NUM_KILLER_MOVES-1; i >= 0; i--)
      {
        if(killerMoves[height][i][0] == move.from && killerMoves[height][i][1] == move.to)
        {
          return true;
        }
      }
      return false;
    }
  };

  class HistoryHeuristic
  {
    int32_t table[2][64][64];
    uint64_t counter;
  public:

    HistoryHeuristic()
    {
      std::fill_n(&table[0][0][0], 2*64*64, 0);
      counter = 1;
    }

    void clear()
    {
      std::fill_n(&table[0][0][0], 2*64*64, 0);
      counter = 1;
    }

    void update(const Move& move, Ply depth, Ply height, Player us)
    {
      if(height <= MAX_HEIGHT_HISTORY_HEURISTIC && not move.isTactical())
      {
        table[us][move.from][move.to] += depth;
        counter += 1;
      }
    }

    int32_t getValue(const Move& move, Player us)
    {
      return table[us][move.from][move.to] / (counter/100.0);
    }
  };

  class MoveHistory
  {
    uint64_t history[MAX_DEPTH];
    std::vector<uint64_t> staticHistory;
  public:

    MoveHistory(const std::vector<Position>& history)
    {
      staticHistory = std::vector<uint64_t>();
      for(size_t i = 0; i+1<history.size(); i++)
      {
        const auto& position = history[i];
        staticHistory.push_back(position.zobristKey);
      }
    }

    void update(uint64_t zobristKey, Ply height)
    {
      history[height] = zobristKey;
    }
    bool checkForRepetition(uint64_t zobristKey, Ply height)
    {
      for(ssize_t i = 0; i<height; i++)
      {
        if(zobristKey == history[i])
        {
          return true;
        }
      }
      for(uint64_t i : staticHistory)
      {
        if(zobristKey == i)
        {
          return true;
        }
      }
      return false;
    }
  };

  struct SearchStruct
  {
    TranspositionTable* tt;
    KillerMoveTable* killer;
    HistoryHeuristic* hh;
    MoveHistory* history;
    PvTable* pv;
    const std::atomic<bool>* shouldStop;
    uint64_t nodes;
    Ply selDepth;
  };

  int32_t evaluateMove(const Move& move, const SearchStruct& ss, const Position& position, Ply depth, Ply height, const Move& expectedBestMove)
  {
    /*
    1. pv move from previous iteration
    2. full transposition moves
    3. winning captures
    4. captures
    5. killermoves
    6. rest ordered with history heuristic
    */
    int32_t value;

    if(ss.pv->isPv(move.zobristKey, height))
    {
      value = 2100000000;
      return value;
    }

    if(ss.tt->getValue(move.zobristKey, depth-1).value != NO_VALUE)
    {
      value = 2090000000;
      return value;
    }

    if(expectedBestMove.to == move.to && expectedBestMove.from == move.from)
    {
      value = 2080000000;
      return value;
    }

    if(move.isTactical())
    {
      value = 2070000000;
      value += move.MVV_LVA();

      if(VALUE[move.moved] > VALUE[move.captured])
      {
        Position newPosition = position;
        newPosition.doMove(move);
        Value seeValue = VALUE[move.captured] - newPosition.see(move.to, move.moved);
        if(seeValue <= 0)
        {
          value -= VALUE[move.moved]/10;
        }
      }

      return value;
    }
    else if(ss.killer->isKillerMove(move, height) || ss.tt->failedHigh(move.zobristKey))
    {
      value = 2060000000;
      return value;
    }
    else
    {
      value = ss.hh->getValue(move, position.us);
      ASSERT(value >= 0);
      ASSERT(value < 2000000000);
      return value;
    }
  }
  int32_t evaluateMove(const Move& move)
  {
    Value value = 0;
    if(move.isTactical())
    {
      value += move.MVV_LVA();
    }

    return value;
  }

  Value quiesce(
    SearchStruct& ss,
    const Position& position,
    Value alpha,
    Value beta,
    Ply height,
    int8_t numberChecks
  )
  {
    constexpr int MAX_NUM_CHECKS_EXTENSIONS = 20;
    Value standPat;
    bool inCheck;
    MoveList moveList;
    Value value;
    Position newPosition;
    std::optional<Move> move;
    int moveCounter = 0;
    ss.nodes += 1;

    if(*ss.shouldStop)
    {
      return 0;
    }

    auto result = ss.tt->getValue(position.zobristKey, 0);
    if(result.value != NO_VALUE)
    {
      if(result.nodeType == PV_NODE)
      {
        return result.value;
      }
      else if(result.nodeType == CUT_NODE && alpha < result.value)
      {
        alpha = result.value;
      }
      else if(result.nodeType == ALL_NODE && beta > result.value)
      {
        beta = result.value;
      }
    }

    standPat = position.evaluate();
    inCheck = position.inCheck(position.us, position.enemy) && numberChecks < MAX_NUM_CHECKS_EXTENSIONS;
    if(standPat > alpha && not inCheck)
    {
      alpha = standPat;
    }
    if(standPat >= beta)
    {
      return beta;
    }

    if(inCheck)
    {
      moveList = position.generateMoveList();
      numberChecks += 1;
    }
    else
    {
      moveList = position.generateCaptureMoveList();
    }
    moveList.order(evaluateMove);

    while((move = moveList.next()).has_value())
    {
      newPosition = position;
      newPosition.doMove(*move);
      if(newPosition.inCheck(newPosition.enemy, newPosition.us))
      {
        continue;
      }
      moveCounter += 1;

      if(not inCheck && standPat + VALUE[move->captured] - newPosition.see(move->to, move->moved) + DELTA_MARGIN < alpha)
      {
        continue;
      }

      value = -quiesce(ss, newPosition, -beta, -alpha, height+1, numberChecks);

      if(value >= beta)
      {
        return beta;
      }
      if(value > alpha)
      {
        alpha = value;
      }
    }

    if(moveCounter == 0 && inCheck)
    {
      alpha = -VALUE_OF_ALL_PIECES;
    }

    return alpha;
  }

  Value search(
    SearchStruct& ss,
    const Position& position,
    Ply depth,
    Ply height,
    Value alpha,
    Value beta,
    MoveList* fill
  )
  {
    Value value;
    const bool inCheck = position.inCheck(position.us, position.enemy);
    bool givingCheck = false;
    MoveList moveList;
    bool generatedMoveList = false;
    NodeType nodeType = ALL_NODE;
    Position newPosition;
    Move bestMove = NO_MOVE;
    std::optional<Move> move;
    bool doFutilityPruning = false;
    int moveCounter = 0;
    Ply newDepth;
    ss.nodes += 1;

    if(*ss.shouldStop)
    {
      return 0;
    }

    if(ss.history->checkForRepetition(position.zobristKey, height) && height > 0)
    {
      ss.pv->clear(height);
      return VALUE_DRAW;
    }
    ss.history->update(position.zobristKey, height);

    // check extensions
    if(inCheck)
    {
      depth += 1;
    }

    // transposition table lookup
    auto result = ss.tt->getValue(position.zobristKey, depth);
    if(result.value != NO_VALUE)
    {
      if(result.nodeType == PV_NODE)
      {
        return result.value;
      }
      else if(result.nodeType == CUT_NODE && alpha < result.value)
      {
        alpha = result.value;
      }
      else if(result.nodeType == ALL_NODE && beta > result.value)
      {
        beta = result.value;
      }
    }
    else
    {
      result = ss.tt->getValue(position.zobristKey, depth - 3);
    }
    // internal iterativ deepening in case no expected best move has been found
    if(result.value == NO_VALUE && depth > 3)
    {
      search(ss, position, depth-2, height, alpha, beta, &moveList);
      generatedMoveList = true;
      result = ss.tt->getValue(position.zobristKey, depth-2);
    }
    if(result.value != NO_VALUE) // TODO: make better
    {
      bestMove.to = result.bestMoveTo;
      bestMove.from = result.bestMoveFrom;
    }

    if(depth <= 0)
    {
      ss.selDepth = std::max(height, ss.selDepth);
      return quiesce(ss, position, alpha, beta, height, 0);
    }

    // null move pruning
    const int8_t numberPiecesExceptKingAndPawnForUs =
      countOnes(
        (position.pieces[KNIGHT] | position.pieces[BISHOP] | position.pieces[ROOK] | position.pieces[QUEEN]) & position.players[position.us]
      );
    if(
      height > 0 &&
      not inCheck &&
      numberPiecesExceptKingAndPawnForUs >= NULL_MOVE_MIN_NUMBER_PIECES_EXCEPT_PAWN_AND_KING
    )
    {
      newPosition = position;
      newPosition.doNullMove();

      value = -search(ss, newPosition, depth - 1 - NULL_MOVE_DEPTH_REDUCTION, height + 2, -beta, -beta+1, nullptr);

      if(value >= beta)
      {
        depth = depth - NULL_MOVE_DEPTH_REDUCTION;
        alpha = value;
        nodeType = CUT_NODE;
        goto Search_Return;
      }
    }

    if(
      depth == 1 &&
      not inCheck &&
      std::abs(alpha) < VALUE_OF_ALL_PIECES &&
      position.evaluate() + FUTILITY_MARGIN < alpha
    )
    {
      doFutilityPruning = true;
    }

    if(not generatedMoveList)
    {
      moveList = position.generateMoveList();
    }
    moveList.order<const SearchStruct&, const Position&, Ply, Ply, const Move&>(evaluateMove, ss, position, depth, height, bestMove);

    // search through moves
    while((move = moveList.next()).has_value())
    {
      newPosition = position;
      newPosition.doMove(*move);
      if(newPosition.inCheck(newPosition.enemy, newPosition.us)) // are we in check after we make our move?
      {
        continue;
      }

      moveCounter += 1;

      givingCheck = newPosition.inCheck(newPosition.us, newPosition.enemy);

      // futility pruning
      if(
        doFutilityPruning &&
        not move->isTactical() &&
        not givingCheck
      )
      {
        continue;
      }

      newDepth = depth;

      /*// late move reduction
      if(
        depth >= 2 &&
        moveCounter > LMR_MOVE_INDEX &&
        not move->isTactical() &&
        not inCheck &&
        not givingCheck
      )
      {
        newDepth -= 1;
        if(moveCounter-LMR_MOVE_INDEX > 10)
        {
          newDepth -= depth / 3;
        }
      }

      if(nodeType != PV_NODE) // TODO: verify pvs search
      {
        value = -search(ss, newPosition, newDepth - 1, height + 1, -beta, -alpha);
      }
      else
      {
        value = -search(ss, newPosition, newDepth - 1, height + 1, -alpha-1, -alpha);
        if(value > alpha)
        {
            value = -search(ss, newPosition, newDepth - 1, height + 1, -beta, -alpha);
        }
      }

      if(
        newDepth < depth &&
        value > alpha
      )
      {
        value = -search(ss, newPosition, depth - 1, height + 1, -beta, -alpha);
      }*/

      // late move reduction
      if(
        depth >= 2 &&
        moveCounter > LMR_MOVE_INDEX &&
        not move->isTactical() &&
        not inCheck &&
        not givingCheck
      )
      {
        Ply lmrDepth = depth - 1;
        if(moveCounter-LMR_MOVE_INDEX > 10)
        {
          lmrDepth -= depth / 3;
        }
        value = -search(ss, newPosition, lmrDepth - 1, height + 1, -alpha-1, -alpha, nullptr);
        if(value <= alpha)
        {
          continue;
        }
      }

      value = -search(ss, newPosition, depth - 1, height + 1, -beta, -alpha, nullptr);
      if(fill != nullptr)
      {
        moveList.setValue(value);
      }


      if(value >= beta)
      {
        nodeType = CUT_NODE;
        ss.killer->update(*move, height);
        ss.tt->setFailedHigh(move->zobristKey);
        ss.hh->update(*move, depth, height, position.us);
        alpha = value;
        bestMove = *move;
        goto Search_Return;
      }
      if(value > alpha)
      {
        bestMove = *move;
        nodeType = PV_NODE;
        ss.pv->update(*move, height);
        alpha = value;
      }
    }

    if(moveCounter == 0)
    {
      if(inCheck)
      {
        alpha = -(VALUE_MATE + (MAX_DEPTH-height));
      }
      else
      {
        alpha = VALUE_DRAW;
      }
    }

    Search_Return:

    if(fill != nullptr)
    {
      if(moveList.size() == 0)
      {
        moveList = position.generateMoveList();
      }
      moveList.reset();
      ASSERT(moveList.size() != 0);
      *fill = moveList;
      ASSERT(fill->size() != 0);
    }

    if(nodeType != PV_NODE)
    {
      ss.pv->clear(height);
    }
    else
    {
      ss.hh->update(bestMove, depth, height, position.us);
    }

    ss.tt->add(position.zobristKey, alpha, depth, nodeType, bestMove);

    return alpha;
  }

  std::string getInfoString(
    Ply depth,
    Ply selDepth,
    int timeElapsed,
    uint64_t nodes,
    Value alpha,
    PvTable pv,
    size_t hashFull,
    float branchingFactor
  )
  {
    std::string ret = "";
    ret += "info";
    ret += " depth " + std::to_string(depth);
    ret += " seldepth " + std::to_string(selDepth);
    ret += " time " + std::to_string(timeElapsed);
    ret += " nodes " + std::to_string(nodes);
    ret += " nps " + std::to_string((int)(nodes / (0.001 * timeElapsed)));
    ret += " ebf " + std::to_string(branchingFactor);
    ret += " hashfull " + std::to_string(hashFull);

    if(alpha >= VALUE_MATE)
    {
      ret += " score mate " + std::to_string((-(alpha - (VALUE_MATE+MAX_DEPTH))+1)/2);
    }
    else if(alpha <= -VALUE_MATE)
    {
      ret += " score mate " + std::to_string(((-alpha - (VALUE_MATE+MAX_DEPTH))-1)/2);
    }
    else
    {
      ret += " score cp " + std::to_string(alpha);
      ret += " cpuload " + std::to_string((int)(valueToProbability(alpha)*1000));
    }
    ret += " pv ";
    ret += pv.getPvString();
    return ret;
  }

  Move rootSearch(
    Position position,
    Ply maxDepth,
    std::vector<Position> history,
    TranspositionTable& tt,
    int moveTime,
    const std::atomic<bool>& shouldStop
  )
  {
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    KillerMoveTable killer = KillerMoveTable();
    HistoryHeuristic hh = HistoryHeuristic();
    MoveHistory moveHistory = MoveHistory(history);
    PvTable pv = PvTable();
    tt.clear();

    SearchStruct ss = {};
    ss.shouldStop = &shouldStop;
    ss.tt = &tt;
    ss.killer = &killer;
    ss.hh = &hh;
    ss.history = &moveHistory;
    ss.pv = &pv;
    ss.nodes = 0;
    ss.selDepth = 0;

    Move bestMove = NO_MOVE;
    uint64_t nodes_l = -1;
    float branchingFactors[MAX_DEPTH];

    for(Ply depth = 1; depth<=maxDepth; depth++)
    {
      std::chrono::steady_clock::time_point iterationStart = std::chrono::steady_clock::now();

      ss.nodes = 0;
      Value alpha = -VALUE_INFINITY;
      const Value beta = VALUE_INFINITY;

      alpha = search(ss, position, depth, 0, alpha, beta, nullptr);

      if(*ss.shouldStop)
      {
        break;
      }
      else
      {
        bestMove = ss.pv->getPvMove(0);
      }

      std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
      auto timeLastIteration = std::chrono::duration_cast<std::chrono::milliseconds>(end - iterationStart).count();
      auto timeElapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

      branchingFactors[depth] = (float)ss.nodes / (float)nodes_l;

      std::cout << getInfoString(
        depth,
        ss.selDepth,
        timeLastIteration,
        ss.nodes,
        alpha,
        *ss.pv,
        ss.tt->hashFull(),
        branchingFactors[depth]
      ) << std::endl;

      if(alpha >= VALUE_MATE || alpha <= -VALUE_MATE)
      {
        break;
      }

      if(moveTime >= 0)
      {
        float averangedBranchingFactor = branchingFactors[depth];
        if(depth >= 4)
        {
          averangedBranchingFactor = (branchingFactors[depth]+branchingFactors[depth-1]+branchingFactors[depth-2]+branchingFactors[depth-3])/4.0;
        }

        auto estimatedTimeNextIteration = timeLastIteration*averangedBranchingFactor;
        if(moveTime >= 500)
        {
          estimatedTimeNextIteration = (estimatedTimeNextIteration*75)/100;
        }
        if(
          (estimatedTimeNextIteration > moveTime && depth >= 6) ||
          timeElapsed > moveTime
        )
        {
          break;
        }
      }

      nodes_l = ss.nodes;
    }

    std::cout << "bestmove " << bestMove.getNotation() << std::endl;
    return bestMove;
  }

  int calculateMoveTime(int millisecondsLeft, int millisecondsIncPerMove, int movesToGo, int fullmovesPlayed)
  {
    const int ESTIMATED_GAME_LENGTH = 80;
    if(movesToGo == -1)
    {
    	movesToGo = std::max(ESTIMATED_GAME_LENGTH - fullmovesPlayed, 20);
    }
    else if(movesToGo > std::max(ESTIMATED_GAME_LENGTH - fullmovesPlayed, 30))
    {
      movesToGo = std::max(ESTIMATED_GAME_LENGTH - fullmovesPlayed, 20);
    }

    if(movesToGo <= 40)
    {
      movesToGo = 2 + (100*movesToGo)/105;
    }

    return millisecondsLeft/movesToGo + millisecondsIncPerMove;
  }
}

Move startSearch(
  TranspositionTable& tt,
  std::vector<Position> history,
  Position position,
  Ply maxDepth,
  int msLeft,
  int msIncPerMove,
  int movesToGo,
  std::atomic<bool>& shouldStop
)
{
  int moveTime = calculateMoveTime(msLeft, msIncPerMove, movesToGo, position.fullmovesPlayed);

  if(msLeft >= 0)
  {
    std::thread stopWatch(Util::setIn, msLeft/2, std::ref(shouldStop));
    stopWatch.detach();
  }

  auto ret = rootSearch(
    position,
    maxDepth,
    history,
    tt,
    moveTime,
    shouldStop
  );

  shouldStop = true;

  return ret;
}
