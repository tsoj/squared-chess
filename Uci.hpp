#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <cstdlib>
#include <optional>
#include <limits>
#include <chrono>
#include <thread>

#include "Position.hpp"
#include "Search.hpp"
#include "ChessTypes.hpp"
#include "ChessData.hpp"
#include "uciInterface.hpp"
#include "Util.hpp"

using namespace ChessTypes;
using namespace ChessData;

namespace Uci
{
  const std::string CHESS_ENGINE_VERSION_NUMBER = "1.3.0";
  const std::string CHESS_ENGINE_NAME = "^2-chess";
  const std::string CHESS_ENGINE_AUTHOR = "Tsoj Tsoj";
  const std::string STARTPOS_FEN = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
  constexpr size_t DEFAULT_HASH_SIZE = 140;
  constexpr size_t MB = 1'000'000;

  std::vector<std::string> split(const std::string &s, char delim)
  {
    std::vector<std::string> elements;
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim))
    {
      if(item != "")
      {
        elements.push_back(item);
      }
    }
    return elements;
  }

  void uci()
  {
      std::cout << "id name " << CHESS_ENGINE_NAME << " " << CHESS_ENGINE_VERSION_NUMBER << std::endl;
      std::cout << "id author " << CHESS_ENGINE_AUTHOR << std::endl;
      std::cout << "option name Hash type spin default " << DEFAULT_HASH_SIZE << " min 1 max 1048576" << std::endl;
      std::cout << "uciok" << std::endl;
  }

  struct UciState
  {
    Position position;
    TranspositionTable tt;
    std::vector<Position> history;
    std::atomic<bool> shouldStop;
  };

  void setoption(const std::vector<std::string>& params, UciState& uciState)
  {
    if(
      params.size() == 5 &&
      params[1] == "name" &&
      params[2] == "Hash" &&
      params[3] == "value"
    )
    {
      size_t newHashSize = std::strtoull(params[4].c_str(), nullptr, 10);
      uciState.tt = TranspositionTable(newHashSize*MB);
    }
    else
    {
      std::cout << "Unkown parameters" << std::endl;
    }
  }
  void stop(UciState& uciState)
  {
    uciState.shouldStop = true;
  }
  Move getMove(const std::string moveString, const Position position)
  {
    auto us = position.us;
    auto newMove = NO_MOVE;
    newMove.from = getSquareIndex(std::string(moveString, 0, 2));
    newMove.to = getSquareIndex(std::string(moveString, 2, 2));
    if(moveString.size() == 5)
    {
      newMove.promoted = std::get<1>(getPiece(std::string(moveString, 4, 1)));
    }
    for(Piece i = 0; i<NO_PIECE; i++)
    {
      if((position.pieces[i] & BIT_AT_INDEX[newMove.from]) != 0)
      {
        newMove.moved = i;
      }
      if((position.pieces[i] & BIT_AT_INDEX[newMove.to]) != 0)
      {
        newMove.captured = i;
      }
    }
    newMove.enPassantCastling = position.enPassantCastling & (RANKS[0] | RANKS[7]);
    //captured en passant
    if(
      newMove.moved == PAWN &&
      (BIT_AT_INDEX[newMove.to] & (position.enPassantCastling & (RANKS[2] | RANKS[5]))) != 0
    )
    {
      newMove.capturedEnPassant = true;
    }
    //pawn double push
    if(
      newMove.moved == PAWN &&
      (BIT_AT_INDEX[newMove.from] & PAWN_HOME_RANK[us]) != 0 &&
      (BIT_AT_INDEX[newMove.to] & (PAWN_QUIET_ATTACK_TABLE[us][newMove.from] | PAWN_CAPTURE_ATTACK_TABLE[us][newMove.from])) == 0
    )
    {
      newMove.enPassantCastling |= PAWN_QUIET_ATTACK_TABLE[us][newMove.from];
    }
    //castling
    //queenside
    if(
      newMove.from == CASTLING_KING_FROM_SQUARE[us] &&
      newMove.to == CASTLING_QUEENSIDE_KING_TO_SQUARE[us] &&
      (position.enPassantCastling & CASTLING_QUEENSIDE_ROOK_FROM[us]) != 0 &&
      (position.enPassantCastling & CASTLING_KING_FROM[us]) != 0
    )
    {
      newMove.enPassantCastling &= ~(CASTLING_QUEENSIDE_ROOK_FROM[us] | CASTLING_KING_FROM[us]);
      newMove.castled = true;
    }
    //kingside
    if(
      newMove.from == CASTLING_KING_FROM_SQUARE[us] &&
      newMove.to == CASTLING_KINGSIDE_KING_TO_SQUARE[us] &&
      (position.enPassantCastling & CASTLING_KINGSIDE_ROOK_FROM[us]) != 0 &&
      (position.enPassantCastling & CASTLING_KING_FROM[us]) != 0
    )
    {
      newMove.enPassantCastling &= ~(CASTLING_KINGSIDE_ROOK_FROM[us] | CASTLING_KING_FROM[us]);
      newMove.castled = true;
    }
    newMove.enPassantCastling &= ~(BIT_AT_INDEX[newMove.from] & (RANKS[0] | RANKS[7]));
    newMove.enPassantCastling &= ~(BIT_AT_INDEX[newMove.to] & (RANKS[0] | RANKS[7]));
    newMove.zobristKey = position.getUpdatedZobristKey(newMove);

    return newMove;
  }
  void position(const std::vector<std::string>& params, UciState& uciState)
  {
    uciState.history = std::vector<Position>();
    size_t index = 0;
    if(params.size() >= 2 && params[1] == "startpos")
    {
      uciState.position.setFromFen(STARTPOS_FEN);
      uciState.history.push_back(uciState.position);
      index = 2;
    }
    else if(params.size() >= 8 && params[1] == "fen")
    {
      std::string fen = params[2] + " " + params[3] + " " + params[4] + " " + params[5] + " " + params[6] + " " + params[7];
      uciState.position.setFromFen(fen);
      uciState.history.push_back(uciState.position);
      index = 8;
    }
    else
    {
      std::cout << "Unkown parameter" << std::endl;
      return;
    }

    if(params.size() >= index+1 && params[index] == "moves")
    {
      for(size_t i = index+1; i<params.size(); i++)
      {
        Move move = getMove(params[i], uciState.position);
        uciState.position.doMove(move);
        uciState.history.push_back(uciState.position);
      }
    }
    else if(params.size() >= 9)
    {
      std::cout << "Unkown parameter" << std::endl;
    }
  }
  void moves(const std::vector<std::string>& params, UciState& uciState)
  {
    for(size_t i = 1; i<params.size(); i++)
    {
      Move move = getMove(params[i], uciState.position);
      uciState.position.doMove(move);
      uciState.history.push_back(uciState.position);
    }
  }
  void go(const std::vector<std::string>& params, UciState& uciState)
  {
    int depth = 64;
    std::optional<int> wtime = {};
  	std::optional<int> btime = {};
  	int winc = 0;
  	int binc = 0;
  	int movestogo = -1;
    std::optional<int> movetime = {};
    std::optional<int> nodes = {};

    for(size_t i = 1; i+1<params.size(); i++)
    {
      if(params[i] == "depth")
      {
        i += 1;
        depth = std::strtol(params[i].c_str(), NULL, 10);
      }
      else if(params[i] == "wtime")
      {
        i += 1;
        wtime = std::strtol(params[i].c_str(), NULL, 10);
      }
      else if(params[i] == "btime")
      {
        i += 1;
        btime = std::strtol(params[i].c_str(), NULL, 10);
      }
      else if(params[i] == "winc")
      {
        i += 1;
        winc = std::strtol(params[i].c_str(), NULL, 10);
      }
      else if(params[i] == "binc")
      {
        i += 1;
        binc = std::strtol(params[i].c_str(), NULL, 10);
      }
      else if(params[i] == "movestogo")
      {
        i += 1;
        movestogo = std::strtol(params[i].c_str(), NULL, 10);
      }
      else if(params[i] == "movetime")
      {
        i += 1;
        movetime = std::strtol(params[i].c_str(), NULL, 10);
      }
      else if(params[i] == "nodes")
      {
        i += 1;
        nodes = std::strtol(params[i].c_str(), NULL, 10);
      }
      else
      {
        std::cout << "Unkown parameter" << std::endl;
        return;
      }
    }

    int32_t _timeLeft = INT32_MAX;
    int32_t _inc = INT32_MAX;
    if(uciState.position.us == WHITE)
    {
      if(wtime.has_value())
      {
        _timeLeft = wtime.value();
        _inc = winc;
      }
    }
    else if(uciState.position.us == BLACK)
    {
      if(wtime.has_value())
      {
        _timeLeft = btime.value();
        _inc = binc;
      }
    }

    if(movetime.has_value())
    {
      std::thread stopWatch(Util::setIn, movetime.value(), std::ref(uciState.shouldStop));
      stopWatch.detach();
    }


    uciState.shouldStop = false;

    std::thread search(startSearch,
      std::ref(uciState.tt),
      uciState.history,
      uciState.position,
      depth,
      _timeLeft,
      _inc,
      movestogo,
      std::ref(uciState.shouldStop)
    );

    search.detach();
  }

  void uciLoop()
  {
    UciState uciState = UciState{Position(), TranspositionTable(DEFAULT_HASH_SIZE*MB), std::vector<Position>(), false};
    uciState.position.setFromFen(STARTPOS_FEN);

    while(true)
    {
      std::string command;
      std::getline(std::cin, command, '\n');

      std::vector<std::string> params = split(command, ' ');

      if(params.size() <= 0)
      {
        continue;
      }
      if(params[0] == "uci")
      {
        uci();
      }
      else if(params[0] == "isready")
      {
        std::cout << "readyok" << std::endl;
      }
      else if(params[0] == "setoption")
      {
        setoption(params, uciState);
      }
      else if(params[0] == "position")
      {
        position(params, uciState);
      }
      else if(params[0] == "go")
      {
        go(params, uciState);
      }
      else if(params[0] == "stop")
      {
        stop(uciState);
      }
      else if(params[0] == "quit")
      {
        stop(uciState);
        break;
      }
      else if(params[0] == "print")
      {
        std::cout << uciState.position.getString() << std::endl;
      }
      else if(params[0] == "printdebug")
      {
        std::cout << uciState.position.getDataString() << std::endl;
      }
      else if(params[0] == "ucinewgame")
      {

      }
      else if(params[0] == "moves")
      {
        moves(params, uciState);
      }
      else if(params[0] == "help")
      {
        std::cout << uciInterface << std::endl;
      }
      else
      {
        std::cout << "Unknown command: " << params[0] << std::endl;
      }

    }

  }

}
