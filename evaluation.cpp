#include "Position.hpp"

#include "ChessData.hpp"
#include "Position.hpp"
#include "Move.hpp"
#include "BitOperations.hpp"

#include <array>
#include <algorithm>

namespace
{
  using namespace ChessData;
  using namespace BitOperations;

  typedef int8_t Phase;
  constexpr Phase OPENING = 32;
  constexpr Phase ENDGAME = 0;
  Phase getPhase(const Position& position)
  {
    const int8_t material = countOnes(position.players[WHITE] | position.players[BLACK]);
    return std::clamp((Phase)((material*32) / 13 - 18), ENDGAME, OPENING);
  }

  typedef std::array<std::array<std::array<std::array<Value, 64>, 2>, 6>, 2> Pst;
  Pst genPst()
  {
    const Value pstBlack[6][2][64] =
    {
      // PAWN
      {
        // OPENING
        {
           0,  0,  0,  0,  0,  0,  0,  0,
          45, 45, 45, 45, 45, 45, 45, 45,
          10, 10, 20, 30, 30, 20, 10, 10,
           5,  5, 10, 25, 25, 10,  5,  5,
           0,  0,  0, 20, 20,  0,  0,  0,
           5, -5,-10,  0,  0,-10, -5,  5,
           5, 10, 10,-20,-20, 10, 10,  5,
           0,  0,  0,  0,  0,  0,  0,  0
        },
        // ENDGAME
        {
           0,  0,  0,  0,  0,  0,  0,  0,
          90, 90, 90, 90, 90, 90, 90, 90,
          30, 30, 40, 45, 45, 40, 40, 30,
          20, 20, 20, 25, 25, 20, 20, 20,
           0,  0,  0, 20, 20,  0,  0,  0,
          -5, -5,-10,-10,-10,-10, -5, -5,
         -15,-15,-15,-20,-20,-15,-15,-15,
           0,  0,  0,  0,  0,  0,  0,  0
        }
      },
      // KNIGHT
      {
        // OPENING
        {
          -50,-40,-30,-30,-30,-30,-40,-50,
          -40,-20,  0,  0,  0,  0,-20,-40,
          -30,  0, 10, 15, 15, 10,  0,-30,
          -30,  5, 15, 20, 20, 15,  5,-30,
          -30,  0, 15, 20, 20, 15,  0,-30,
          -30,  5, 10, 15, 15, 10,  5,-30,
          -40,-20,  0,  5,  5,  0,-20,-40,
          -50,-40,-30,-30,-30,-30,-40,-50,
        },
        // ENDGAME
        {
          -50,-40,-30,-30,-30,-30,-40,-50,
          -40,-20,  0,  0,  0,  0,-20,-40,
          -30,  0, 10, 15, 15, 10,  0,-30,
          -30,  5, 15, 20, 20, 15,  5,-30,
          -30,  0, 15, 20, 20, 15,  0,-30,
          -30,  5, 10, 15, 15, 10,  5,-30,
          -40,-20,  0,  5,  5,  0,-20,-40,
          -50,-40,-30,-30,-30,-30,-40,-50,
        }
      },
      // BISHOP
      {
        // OPENING
        {
          -20,-10,-10,-10,-10,-10,-10,-20,
          -10,  0,  0,  0,  0,  0,  0,-10,
          -10,  0,  5, 10, 10,  5,  0,-10,
          -10,  5,  5, 10, 10,  5,  5,-10,
          -10,  0, 10, 10, 10, 10,  0,-10,
          -10, 10, 10, 10, 10, 10, 10,-10,
          -10,  5,  0,  0,  0,  0,  5,-10,
          -20,-10,-10,-10,-10,-10,-10,-20,
        },
        // ENDGAME
        {
          -20,-10,-10,-10,-10,-10,-10,-20,
          -10,  0,  0,  0,  0,  0,  0,-10,
          -10,  0,  5, 10, 10,  5,  0,-10,
          -10,  5,  5, 10, 10,  5,  5,-10,
          -10,  0, 10, 10, 10, 10,  0,-10,
          -10, 10, 10, 10, 10, 10, 10,-10,
          -10,  5,  0,  0,  0,  0,  5,-10,
          -20,-10,-10,-10,-10,-10,-10,-20,
        }
      },
      // ROOK
      {
        // OPENING
        {
           0,  0,  0,  0,  0,  0,  0,  0,
           5, 10, 10, 10, 10, 10, 10,  5,
          -5,  0,  0,  0,  0,  0,  0, -5,
          -5,  0,  0,  0,  0,  0,  0, -5,
          -5,  0,  0,  0,  0,  0,  0, -5,
          -5,  0,  0,  0,  0,  0,  0, -5,
          -5,  0,  0,  0,  0,  0,  0, -5,
           0,  0,  0,  5,  5,  0,  0,  0
        },
        // ENDGAME
        {
           0,  0,  0,  0,  0,  0,  0,  0,
           0,  5,  5,  5,  5,  5,  5,  0,
          -5,  0,  0,  0,  0,  0,  0, -5,
          -5,  0,  0,  0,  0,  0,  0, -5,
          -5,  0,  0,  0,  0,  0,  0, -5,
          -5,  0,  0,  0,  0,  0,  0, -5,
          -5,  0,  0,  0,  0,  0,  0, -5,
           0,  0,  0,  0,  0,  0,  0,  0
        }
      },
      // QUEEN
      {
        // OPENING
        {
          -20,-10,-10, -5, -5,-10,-10,-20,
          -10,  0,  0,  0,  0,  0,  0,-10,
          -10,  0,  5,  5,  5,  5,  0,-10,
           -5,  0,  5,  5,  5,  5,  0, -5,
            0,  0,  5,  5,  5,  5,  0, -5,
          -10,  5,  5,  5,  5,  5,  0,-10,
          -10,  0,  5,  0,  0,  0,  0,-10,
          -20,-10,-10, -5, -5,-10,-10,-20
        },
        // ENDGAME
        {
          -20,-10,-10, -5, -5,-10,-10,-20,
          -10,  0,  0,  0,  0,  0,  0,-10,
          -10,  0,  5,  5,  5,  5,  0,-10,
           -5,  0,  5,  5,  5,  5,  0, -5,
            0,  0,  5,  5,  5,  5,  0, -5,
          -10,  0,  5,  5,  5,  5,  0,-10,
          -10,  0,  0,  0,  0,  0,  0,-10,
          -20,-10,-10, -5, -5,-10,-10,-20
        }
      },
      // KING
      {
        // OPENING
        {
          -30,-40,-40,-50,-50,-40,-40,-30,
          -30,-40,-40,-50,-50,-40,-40,-30,
          -30,-40,-40,-50,-50,-40,-40,-30,
          -30,-40,-40,-50,-50,-40,-40,-30,
          -20,-30,-30,-40,-40,-30,-30,-20,
          -10,-20,-20,-20,-20,-20,-20,-10,
           20, 20,  0,  0,  0,  0, 20, 20,
           20, 30, 10,  0,  0, 10, 30, 20
        },
        // ENDGAME
        {
          -50,-40,-30,-20,-20,-30,-40,-50,
          -30,-20,-10,  0,  0,-10,-20,-30,
          -30,-10, 20, 30, 30, 20,-10,-30,
          -30,-10, 30, 40, 40, 30,-10,-30,
          -30,-10, 30, 40, 40, 30,-10,-30,
          -30,-10, 20, 30, 30, 20,-10,-30,
          -30,-30,  0,  0,  0,  0,-30,-30,
          -50,-30,-30,-30,-30,-30,-30,-50
        }
      },
    };
    Pst ret = {};
    for(Piece piece = 0; piece<NO_PIECE; piece++)
    {
      for(Phase phase = 0; phase<2; phase++)
      {
        for(Square square = 0; square<NO_SQUARE; square++)
        {
          ret[BLACK][piece][phase][square] = pstBlack[piece][phase][square];
          ret[WHITE][piece][phase][square] = pstBlack[piece][phase][mirror(square)];
        }
      }
    }
    return ret;
  }
  const Pst pst = genPst();
  Value pstValue(Player us, Piece piece, Square square, Phase phase)
  {
    return (pst[us][piece][0][square]*(phase))/32 + (pst[us][piece][1][square]*(32-phase))/32;
  }

  template<Piece piece>
  Value mobility(Square square, const Position& position)
  {
    return countOnes(getAttackMask<piece>(square, position.players[WHITE] | position.players[BLACK]));
  }

  const Value PENALTY_ISOLATED_PAWN = 15;
  const Value PENALTY_DOUBLE_PAWN = 5;
  const Value BONUS_BOTH_BISHOPS = 10;
  const Value BONUS_ROOKS_ARE_CONNECTED = 15;
  const Value PENALTY_KING_UNSAFE = 20;

  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wunused-parameter"

  template<Piece piece>
  Value evaluate(const Position& position, Square square, Player us, Player enemy, Phase phase)
  {
    return 0;
  }

  template<>
  Value evaluate<PAWN>(const Position& position, Square square, Player us, Player enemy, Phase phase)
  {
    Value ret = 0;
    // passed pawn
    if(position.isPassedPawn(us, enemy, square))
    {
      ret += pstValue(us, PAWN, square, phase);
    }
    // isolated pawn
    if(
      (square%8 == 0 || position.pieces[PAWN] & position.players[us] & FILES_64[square - 1]) == 0 &&
      (square%8 == 7 || position.pieces[PAWN] & position.players[us]  & FILES_64[square + 1]) == 0
    )
    {
      ret -= PENALTY_ISOLATED_PAWN;
    }
    // double pawn
    if(((position.pieces[PAWN] & position.players[us] & FILES_64[square - 1])^BIT_AT_INDEX[square]) != 0)
    {
      ret -= PENALTY_DOUBLE_PAWN;
    }
    return ret;
  }
  template<>
  Value evaluate<BISHOP>(const Position& position, Square square, Player us, Player enemy, Phase phase)
  {
    Value ret = 0;
    if((position.players[us] & position.pieces[BISHOP] & ~BIT_AT_INDEX[square]) != 0)
    {
      ret += BONUS_BOTH_BISHOPS;
    }
    ret += mobility<BISHOP>(square, position);
    return ret;
  }
  template<>
  Value evaluate<ROOK>(const Position& position, Square square, Player us, Player enemy, Phase phase)
  {
    Value ret = 0;
    //connected rooks are better
    const uint64_t attackMask = getAttackMask<ROOK>(square, position.players[WHITE] | position.players[BLACK]);
    if((attackMask & position.pieces[ROOK] & position.players[us] & ~BIT_AT_INDEX[square]) != 0)
    {
      ret += BONUS_ROOKS_ARE_CONNECTED;
    }
    ret += mobility<ROOK>(square, position);
    return ret;
  }
  template<>
  Value evaluate<QUEEN>(const Position& position, Square square, Player us, Player enemy, Phase phase)
  {
    Value ret = 0;
    ret += mobility<QUEEN>(square, position);
    return ret;
  }
  template<>
  Value evaluate<KING>(const Position& position, Square square, Player us, Player enemy, Phase phase)
  {
    Value ret = 0;
    if(phase >= 15)
    {
      uint64_t kingAttackedArea = BIT_AT_INDEX[square] | (getAttackMask<KING>(square, 0) & ~position.players[us]);
      while(kingAttackedArea != 0)
      {
        const Square attackedSquareSquare = findAndClearTrailingOne(kingAttackedArea);
        if(position.inCheck(us, enemy, attackedSquareSquare))
        {
          ret -= PENALTY_KING_UNSAFE;
        }
      }
    }
    return ret;
  }

  #pragma GCC diagnostic pop

  template<Piece piece>
  Value evaluateMaterialPiece(const Position& position)
  {
    const Player us = position.us;
    const Player enemy = position.enemy;
    Value ret = 0;
    ret += VALUE[piece]*countOnes(position.players[us] & position.pieces[piece]);
    ret -= VALUE[piece]*countOnes(position.players[enemy] & position.pieces[piece]);
    return ret;
  }
  template<Piece piece>
  Value evaluatePiece(const Position& position, Phase phase)
  {
    const Player us = position.us;
    const Player enemy = position.enemy;
    Value ret = 0;

    uint64_t tmpOccupancy = position.pieces[piece];
    while(tmpOccupancy != 0)
    {
      const Square square = findAndClearTrailingOne(tmpOccupancy);
      if((BIT_AT_INDEX[square] & position.players[us]) != 0)
      {
        ret += VALUE[piece];
        ret += pstValue(us, piece, square, phase);
        ret += evaluate<piece>(position, square, us, enemy, phase);
      }
      else
      {
        ret -= VALUE[piece];
        ret -= pstValue(enemy, piece, square, phase);
        ret -= evaluate<piece>(position, square, enemy, us, phase);
      }
    }
    return ret;
  }
}

Value Position::evaluateMaterial() const
{
  if(this->halfmoveClock >= 100)
  {
    return VALUE_DRAW;
  }
  Value ret = 0;
  ret += evaluateMaterialPiece<PAWN>(*this);
  ret += evaluateMaterialPiece<KNIGHT>(*this);
  ret += evaluateMaterialPiece<BISHOP>(*this);
  ret += evaluateMaterialPiece<ROOK>(*this);
  ret += evaluateMaterialPiece<QUEEN>(*this);
  ret += evaluateMaterialPiece<KING>(*this);
  return ret;
}
Value Position::evaluate() const
{
  if(this->halfmoveClock >= 100)
  {
    return VALUE_DRAW;
  }
  Value ret = 0;
  Phase phase = getPhase(*this);
  ret += evaluatePiece<PAWN>(*this, phase);
  ret += evaluatePiece<KNIGHT>(*this, phase);
  ret += evaluatePiece<BISHOP>(*this, phase);
  ret += evaluatePiece<ROOK>(*this, phase);
  ret += evaluatePiece<QUEEN>(*this, phase);
  ret += evaluatePiece<KING>(*this, phase);
  return ret;
}

// TODO: make that faster
Move Position::getLeastValueableAttacker(Square square, Piece pieceOnSquare) const
{
  const uint64_t occupancy = this->players[WHITE] | this->players[BLACK];
  const Player us = this->us;
  const Player enemy = this->enemy;
  uint64_t attacker;
  Move move;
  move.captured = pieceOnSquare;
  move.capturedEnPassant = false;
  move.castled = false;
  move.enPassantCastling = 0;
  move.to = square;
  move.promoted = NO_PIECE;
  move.zobristKey = 0;

  // PAWN
  attacker = (PAWN_CAPTURE_ATTACK_TABLE[enemy][square] & this->pieces[PAWN] & this->players[us]);
  if(attacker != 0)
  {
    move.from = trailingZeros(attacker);
    move.moved = PAWN;
    return move;
  }
  // KNIGHT
  attacker = (getAttackMask<KNIGHT>(square, occupancy) & this->pieces[KNIGHT] & this->players[us]);
  if(attacker != 0)
  {
    move.from = trailingZeros(attacker);
    move.moved = KNIGHT;
    return move;
  }
  // BISHOP
  attacker = (getAttackMask<BISHOP>(square, occupancy) & this->pieces[BISHOP] & this->players[us]);
  if(attacker != 0)
  {
    move.from = trailingZeros(attacker);
    move.moved = BISHOP;
    return move;
  }
  // ROOK
  attacker = (getAttackMask<ROOK>(square, occupancy) & this->pieces[ROOK] & this->players[us]);
  if(attacker != 0)
  {
    move.from = trailingZeros(attacker);
    move.moved = ROOK;
    return move;
  }
  // QUEEN
  attacker = (getAttackMask<QUEEN>(square, occupancy) & this->pieces[QUEEN] & this->players[us]);
  if(attacker != 0)
  {
    move.from = trailingZeros(attacker);
    move.moved = QUEEN;
    return move;
  }
  // KING
  attacker = (getAttackMask<KING>(square, occupancy) & this->pieces[KING] & players[us]);
  if(attacker != 0)
  {
    move.from = trailingZeros(attacker);
    move.moved = KING;
    return move;
  }
  move.moved = NO_PIECE;
  return move;
}

Value Position::see(Square square, Piece pieceOnSquare) const
{
  Value value = 0;

  Position nPosition = *this;
  Move move = nPosition.getLeastValueableAttacker(square, pieceOnSquare);
  if(move.moved != NO_PIECE)
  {
    nPosition.doMove(move);
    Value currentValue = VALUE[pieceOnSquare] - nPosition.see(square, move.moved);
    if(currentValue > 0)
    {
      value = currentValue;
    }
  }

  return value;
}
