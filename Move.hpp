#pragma once

#include "ChessTypes.hpp"
#include "ChessData.hpp"
#include "Position.hpp"

using namespace ChessTypes;
using namespace ChessData;

struct Move
{
  Square from;
  Square to;
  Piece moved;
  Piece captured;
  Piece promoted;
  uint64_t enPassantCastling;
  bool castled;
  bool capturedEnPassant;
  uint64_t zobristKey;

  std::string getString() const
  {
    std::string ret = "";
    ret += "from: "+std::to_string(from)+"\n";
    ret += "to: "+std::to_string(to)+"\n";
    ret += "moved: "+getPieceNotation(WHITE, moved)+"\n";
    ret += "captured: "+getPieceNotation(WHITE, captured)+"\n";
    ret += "promoted: "+getPieceNotation(WHITE, promoted)+"\n";
    ret += "enPassantCastling: "+getBitboardString(enPassantCastling)+"\n";
    ret += "castled: "+std::to_string(castled)+"\n";
    ret += "capturedEnPassant: "+std::to_string(capturedEnPassant)+"\n";
    ret += "zobristKey: "+std::to_string(zobristKey)+"\n";
    return ret;
  }

  std::string getNotation() const
  {
    std::string ret = "";
    ret += getSquareNotation(from);
    ret += getSquareNotation(to);
    if(promoted != NO_PIECE)
    {
      ret += getPieceNotation(BLACK, promoted);
    }
    return ret;
  }

  Value MVV_LVA() const
  {
    Value value = 0;
    value += VALUE[captured];
    if(promoted != NO_PIECE)
    {
      value += VALUE[promoted] - VALUE_PAWN;
    }
    value -= VALUE[moved]/10;
    return value;
  }

  bool isCapture() const
  {
    if(captured != NO_PIECE)
    {
      return true;
    }
    return false;
  }

  bool isTactical() const
  {
    return
      isCapture() ||
      promoted != NO_PIECE;
  }
};

const Move NO_MOVE =
{
  NO_SQUARE, // from
  NO_SQUARE, // to
  NO_PIECE, // moved
  NO_PIECE, // captured
  NO_PIECE, // promoted
  0, // enPassantCastling
  false, // castled
  false, // capturedEnPassant
  0, // zobristKey
};

struct UndoMove
{
  Move m;
  uint64_t old_zobristKey;
  uint64_t old_enPassantCastling;
};
