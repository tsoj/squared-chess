#pragma once

#include <thread>

namespace Util
{
  inline void setIn(long milliseconds, std::atomic<bool>& set)
  {
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
    long timeElapsed;
    do
    {
      std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
      timeElapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
    } while(timeElapsed <= milliseconds && not set);

    set = true;
  }
}
