#pragma once

#include <array>
#include <algorithm>
#include <functional>
#include <optional>

#include "ChessTypes.hpp"
#include "ChessData.hpp"
#include "debug.hpp"
#include "Position.hpp"
#include "Move.hpp"
#include "BitOperations.hpp"
#include "TranspositionTable.hpp"

using namespace ChessTypes;
using namespace ChessData;
using namespace BitOperations;

struct MoveList
{
  private:

  static const size_t MOVE_LIST_MAXIMUM_SIZE = 128;

  size_t currentSize;
  std::array<Move, MOVE_LIST_MAXIMUM_SIZE> moves;
  std::array<int32_t, MOVE_LIST_MAXIMUM_SIZE> orderingValues;
  std::array<bool, MOVE_LIST_MAXIMUM_SIZE> done;

  size_t last;

  public:

  MoveList() : currentSize(0){}

  size_t size() const
  {
    return currentSize;
  }

  Move& operator[](const size_t i)
  {
    ASSERT(i < moves.size())
    ASSERT(i < currentSize)
    return moves[i];
  }
  const Move& operator[](const size_t i) const
  {
    ASSERT(i < moves.size())
    ASSERT(i < currentSize)
    return moves[i];
  }

  void addMove(
    const Square from,
    const Square to,
    const Piece moved,
    const Piece captured,
    const Piece promoted,
    const uint64_t enPassantCastling,
    const bool castled,
    const bool capturedEnPassant,
    const Position& origPosition
  )
  {
    moves[currentSize].from = from;
    moves[currentSize].to = to;
    moves[currentSize].moved = moved;
    moves[currentSize].captured = captured;
    moves[currentSize].promoted = promoted;
    moves[currentSize].enPassantCastling = enPassantCastling;
    moves[currentSize].castled = castled;
    moves[currentSize].capturedEnPassant = capturedEnPassant;
    moves[currentSize].zobristKey = origPosition.getUpdatedZobristKey(moves[currentSize]);
    orderingValues[currentSize] = 0;
    done[currentSize] = false;
    currentSize++;
  }

  template<Piece piece>
  void generateMoves(const Position& origPosition, const uint64_t newEnPassantCastling, const bool onlyCaptures);
  void generateCastlingMoves(const Position& origPosition, const uint64_t newEnPassantCastling);

  template<typename... Args>
  void order(int32_t (*moveEvaluation)(const Move&, Args...), Args... args)
  {
    for(size_t i = 0; i < size(); i++)
    {
      orderingValues[i] += moveEvaluation(moves[i], args...);
    }
  }

  std::optional<Move> next()
  {
    std::optional<Move> ret = {};
    int32_t maxValue = INT32_MIN;
    size_t best = MOVE_LIST_MAXIMUM_SIZE;

    for(size_t i = 0; i<size(); i++)
    {
      if(orderingValues[i] > maxValue && not done[i])
      {
        best = i;
        maxValue = orderingValues[i];
      }
    }
    if(best < MOVE_LIST_MAXIMUM_SIZE)
    {
      ret = moves[best];
      last = best;
      done[best] = true;
    }
    return ret;
  }

  void reset()
  {
    for(auto& i : done)
    {
      i = false;
    }
  }

  void setValue(Value value)
  {
    orderingValues[last] = value;
  }
};

template<>
void MoveList::generateMoves<PAWN>(const Position& origPosition, const uint64_t newEnPassantCastling, const bool onlyCaptures);


























//
