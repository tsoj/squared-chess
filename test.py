import chess
import chess.uci
import time

test_movetime = 20000
test_movetime_stockfish = 2000

engine = chess.uci.popen_engine("./bin/squared-chess")
info_handler = chess.uci.InfoHandler()
engine.info_handlers.append(info_handler)

engine.uci()
print(engine.name)

engine_stockfish = chess.uci.popen_engine("asmfish")
info_handler_stockfish = chess.uci.InfoHandler()
engine_stockfish.info_handlers.append(info_handler_stockfish)

engine_stockfish.uci()
print(engine_stockfish.name)

fens = ["rn1qkb1r/pp2pppp/5n2/3p1b2/3P4/1QN1P3/PP3PPP/R1B1KBNR b KQkq - 1 1", # id "CCR02";bm Bc8;
"r1bqk2r/ppp2ppp/2n5/4P3/2Bp2n1/5N1P/PP1N1PP1/R2Q1RK1 b kq - 1 10", # id "CCR03"; bm Nh6; am Ne5;
"r1bqrnk1/pp2bp1p/2p2np1/3p2B1/3P4/2NBPN2/PPQ2PPP/1R3RK1 w - - 1 12", # id "CCR04"; bm b4;
"rnbqkb1r/ppp1pppp/5n2/8/3PP3/2N5/PP3PPP/R1BQKBNR b KQkq - 3 5", # id "CCR05"; bm e5;
"rnbq1rk1/pppp1ppp/4pn2/8/1bPP4/P1N5/1PQ1PPPP/R1B1KBNR b KQ - 1 5", # id "CCR06"; bm Bcx3+;
"r4rk1/3nppbp/bq1p1np1/2pP4/8/2N2NPP/PP2PPB1/R1BQR1K1 b - - 1 12", # id "CCR07"; bm Rfb8;
"rn1qkb1r/pb1p1ppp/1p2pn2/2p5/2PP4/5NP1/PP2PPBP/RNBQK2R w KQkq c6 1 6", # id "CCR08"; bm d5;
"r1bq1rk1/1pp2pbp/p1np1np1/3Pp3/2P1P3/2N1BP2/PP4PP/R1NQKB1R b KQ - 1 9", # id "CCR09"; bm Nd4;
"rnbqr1k1/1p3pbp/p2p1np1/2pP4/4P3/2N5/PP1NBPPP/R1BQ1RK1 w - - 1 11", # id "CCR10"; bm a4;
"rnbqkb1r/pppp1ppp/5n2/4p3/4PP2/2N5/PPPP2PP/R1BQKBNR b KQkq f3 1 3", # id "CCR11"; bm d5;
"r1bqk1nr/pppnbppp/3p4/8/2BNP3/8/PPP2PPP/RNBQK2R w KQkq - 2 6", # id "CCR12"; bm Bxf7+;
"rnbq1b1r/ppp2kpp/3p1n2/8/3PP3/8/PPP2PPP/RNBQKB1R b KQ d3 1 5", # id "CCR13"; am Ne4;
"rnbqkb1r/pppp1ppp/3n4/8/2BQ4/5N2/PPP2PPP/RNB2RK1 b kq - 1 6", # id "CCR14"; am Nxc4;
"r2q1rk1/2p1bppp/p2p1n2/1p2P3/4P1b1/1nP1BN2/PP3PPP/RN1QR1K1 w - - 1 12", # id "CCR15"; bm exf6;
"r1bqkb1r/2pp1ppp/p1n5/1p2p3/3Pn3/1B3N2/PPP2PPP/RNBQ1RK1 b kq - 2 7", # id "CCR16"; bm d5;
"r2qkbnr/2p2pp1/p1pp4/4p2p/4P1b1/5N1P/PPPP1PP1/RNBQ1RK1 w kq - 1 8", # id "CCR17"; am hxg4;
"r1bqkb1r/pp3ppp/2np1n2/4p1B1/3NP3/2N5/PPP2PPP/R2QKB1R w KQkq e6 1 7", # id "CCR18"; bm Bxf6+;
"rn1qk2r/1b2bppp/p2ppn2/1p6/3NP3/1BN5/PPP2PPP/R1BQR1K1 w kq - 5 10", # id "CCR19"; am Bxe6;
"r1b1kb1r/1pqpnppp/p1n1p3/8/3NP3/2N1B3/PPP1BPPP/R2QK2R w KQkq - 3 8", # id "CCR20"; am Ndb5;
"r1bqnr2/pp1ppkbp/4N1p1/n3P3/8/2N1B3/PPP2PPP/R2QK2R b KQ - 2 11", # id "CCR21"; am Kxe6;
"r3kb1r/pp1n1ppp/1q2p3/n2p4/3P1Bb1/2PB1N2/PPQ2PPP/RN2K2R w KQkq - 3 11", # id "CCR22"; bm a4;
"r1bq1rk1/pppnnppp/4p3/3pP3/1b1P4/2NB3N/PPP2PPP/R1BQK2R w KQ - 3 7", # id "CCR23"; bm Bxh7+;
"r2qkbnr/ppp1pp1p/3p2p1/3Pn3/4P1b1/2N2N2/PPP2PPP/R1BQKB1R w KQkq - 2 6", # id "CCR24"; bm Nxe5;
"rn2kb1r/pp2pppp/1qP2n2/8/6b1/1Q6/PP1PPPBP/RNB1K1NR b KQkq - 1 6", # id "CCR25"; am Qxb3;

"1k1r4/pp1b1R2/3q2pp/4p3/2B5/4Q3/PPP2B2/2K5 b - - 0 0", #bm Qd1+; id "BK.01";
"3r1k2/4npp1/1ppr3p/p6P/P2PPPP1/1NR5/5K2/2R5 w - - 0 0", #bm d5; id "BK.02";
"2q1rr1k/3bbnnp/p2p1pp1/2pPp3/PpP1P1P1/1P2BNNP/2BQ1PRK/7R b - - 0 0", #bm f5; id "BK.03";
"rnbqkb1r/p3pppp/1p6/2ppP3/3N4/2P5/PPP1QPPP/R1B1KB1R w KQkq - 0 0", #bm e6; id "BK.04";
"r1b2rk1/2q1b1pp/p2ppn2/1p6/3QP3/1BN1B3/PPP3PP/R4RK1 w - - 0 0", #bm Nd5 a4; id "BK.05";
"2r3k1/pppR1pp1/4p3/4P1P1/5P2/1P4K1/P1P5/8 w - - 0 0", #bm g6; id "BK.06";
"1nk1r1r1/pp2n1pp/4p3/q2pPp1N/b1pP1P2/B1P2R2/2P1B1PP/R2Q2K1 w - - 0 0", #bm Nf6; id "BK.07";
"4b3/p3kp2/6p1/3pP2p/2pP1P2/4K1P1/P3N2P/8 w - - 0 0", #bm f5; id "BK.08";
"2kr1bnr/pbpq4/2n1pp2/3p3p/3P1P1B/2N2N1Q/PPP3PP/2KR1B1R w - - 0 0", #bm f5; id "BK.09";
"3rr1k1/pp3pp1/1qn2np1/8/3p4/PP1R1P2/2P1NQPP/R1B3K1 b - - 0 0", #bm Ne5; id "BK.10";
"2r1nrk1/p2q1ppp/bp1p4/n1pPp3/P1P1P3/2PBB1N1/4QPPP/R4RK1 w - - 0 0", #bm f4; id "BK.11";
"r3r1k1/ppqb1ppp/8/4p1NQ/8/2P5/PP3PPP/R3R1K1 b - - 0 0", #bm Bf5; id "BK.12";
"r2q1rk1/4bppp/p2p4/2pP4/3pP3/3Q4/PP1B1PPP/R3R1K1 w - - 0 0", #bm b4; id "BK.13";
"rnb2r1k/pp2p2p/2pp2p1/q2P1p2/8/1Pb2NP1/PB2PPBP/R2Q1RK1 w - - 0 0", #bm Qd2 Qe1; id "BK.14";
"2r3k1/1p2q1pp/2b1pr2/p1pp4/6Q1/1P1PP1R1/P1PN2PP/5RK1 w - - 0 0", #bm Qxg7+; id "BK.15";
"r1bqkb1r/4npp1/p1p4p/1p1pP1B1/8/1B6/PPPN1PPP/R2Q1RK1 w kq - 0 0", #bm Ne4; id "BK.16";
"r2q1rk1/1ppnbppp/p2p1nb1/3Pp3/2P1P1P1/2N2N1P/PPB1QP2/R1B2RK1 b - - 0 0", #bm h5; id "BK.17";
"r1bq1rk1/pp2ppbp/2np2p1/2n5/P3PP2/N1P2N2/1PB3PP/R1B1QRK1 b - - 0 0", #bm Nb3; id "BK.18";
"3rr3/2pq2pk/p2p1pnp/8/2QBPP2/1P6/P5PP/4RRK1 b - - 0 0", #bm Rxe4; id "BK.19";
"r4k2/pb2bp1r/1p1qp2p/3pNp2/3P1P2/2N3P1/PPP1Q2P/2KRR3 w - - 0 0", #bm g4; id "BK.20";
"3rn2k/ppb2rpp/2ppqp2/5N2/2P1P3/1P5Q/PB3PPP/3RR1K1 w - - 0 0", #bm Nh6; id "BK.21";
"2r2rk1/1bqnbpp1/1p1ppn1p/pP6/N1P1P3/P2B1N1P/1B2QPP1/R2R2K1 b - - 0 0", #bm Bxe4; id "BK.22";
"r1bqk2r/pp2bppp/2p5/3pP3/P2Q1P2/2N1B3/1PP3PP/R4RK1 b kq - 0 0", #bm f6; id "BK.23";
"r2qnrnk/p2b2b1/1p1p2pp/2pPpp2/1PP1P3/PRNBB3/3QNPPP/5RK1 w - - 0 0"] #bm f4; id "BK.24";


points = 0
averangeDepth = 0
counter = 0
missedCheckmate = 0

for f in fens:
    board = chess.Board(fen=f, chess960=False)

    engine_stockfish.position(board)
    bestMove = engine_stockfish.go(
    searchmoves=None,
    ponder=False,
    wtime=None,
    btime=None,
    winc=None,
    binc=None,
    movestogo=None,
    depth=None,
    nodes=None,
    mate=None,
    movetime=test_movetime_stockfish,
    infinite=False,
    async_callback=None
    )
    gettingMated = False
    if info_handler_stockfish.info["score"][1].cp != None:
        score = info_handler_stockfish.info["score"][1].cp
        print("cp1: " + str(info_handler_stockfish.info["score"][1].cp))
    if info_handler_stockfish.info["score"][1].mate != None:
        if info_handler_stockfish.info["score"][1].mate < 0:
            gettingMated = True
        print("cp1: # " + str(info_handler_stockfish.info["score"][1].mate))
        missedCheckmate += 1

    engine.position(board)
    bestMove = engine.go(
    searchmoves=None,
    ponder=False,
    wtime=None,
    btime=None,
    winc=None,
    binc=None,
    movestogo=None,
    depth=None,
    nodes=None,
    mate=None,
    movetime=test_movetime,
    infinite=False,
    async_callback=None
    )
    m = bestMove[0]
    print(m)
    print(info_handler.info["depth"])
    averangeDepth += info_handler.info["depth"]

    we = board.turn

    board.push(chess.Move.from_uci(str(m)))
    engine_stockfish.position(board)
    bestMove = engine_stockfish.go(
    searchmoves=None,
    ponder=False,
    wtime=None,
    btime=None,
    winc=None,
    binc=None,
    movestogo=None,
    depth=None,
    nodes=None,
    mate=None,
    movetime=test_movetime_stockfish,
    infinite=False,
    async_callback=None
    )


    delta_error = 0 #info_handler_stockfish.info["score"][1].cp + score
    if info_handler_stockfish.info["score"][1].cp != None:
        print("cp2:" + str(-info_handler_stockfish.info["score"][1].cp))
        if we == chess.WHITE:
            delta_error = -info_handler_stockfish.info["score"][1].cp - score
        if we == chess.BLACK:
            delta_error = -(-(-info_handler_stockfish.info["score"][1].cp) - (-score))

    if info_handler_stockfish.info["score"][1].mate != None:
        print("cp2: # " + str(-info_handler_stockfish.info["score"][1].mate))
        if gettingMated == False and info_handler_stockfish.info["score"][1].mate >=0:
            missedCheckmate += 1
        if gettingMated == True and info_handler_stockfish.info["score"][1].mate >=0:
            missedCheckmate -= 1
        if gettingMated == False and info_handler_stockfish.info["score"][1].mate <=0:
            missedCheckmate -= 1


    print("delta-cp: " + str(delta_error))
    points += delta_error

    print("i: " + str(counter))

    counter += 1



averangeDepth = averangeDepth/counter
points = points/counter

print("averange depth: " + str(averangeDepth))
print("points: " + str(points))
print("missed checkmates: " + str(missedCheckmate))

while engine.is_alive():
    engine.quit()
while engine_stockfish.is_alive():
    engine_stockfish.quit()
