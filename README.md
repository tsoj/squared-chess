##### Compile:

$ make

##### Run:

$ ./bin/squared-chess

##### Features

- implementation:
  - [x] kindergarten bitboards
- evaluation:
  - [x] piece square tables
  - [x] isolated pawns
  - [x] passed pawns
  - [x] mobility
  - [x] king unsafety
- search:
  - [x] alpha-beta/negamax
  - [x] quiescence search
  - [x] transposition table
  - [x] internal iterativ deepening
  - move ordering:
    - [x] MVV-LVA
    - [x] static exchange evaluation
    - [x] transposition table
    - [x] history heuristic
    - [x] killermoves
  - [x] nullmove pruning
  - [x] late move reductions
  - [x] check extensions
  - [x] delta pruning
