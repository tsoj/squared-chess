CC_WIN	= x86_64-w64-mingw32-g++
CC	= g++
CFLAGS  = -std=c++17 -O4 -Wall -Wextra -Wpedantic
LDFLAGS = -std=c++17 -O4 -Wall -Wextra -Wpedantic -pthread
PLUS_LDFLAGS_WIN = -static -static-libgcc -static-libstdc++
NAME = squared-chess
BIN_FILE_PATH = ./bin/

CPP = main.cpp Position.cpp MoveList.cpp Search.cpp evaluation.cpp

OBJ = $(CPP:%.cpp=$(BIN_FILE_PATH)%.o)

-include $(OBJ:%.o=%.d)

all: build $(OBJ)
	$(CC) -o $(BIN_FILE_PATH)$(NAME) $(OBJ) $(LDFLAGS)

build: $(BIN_FILE_PATH)build.o
	echo Hello World

$(BIN_FILE_PATH)build.o: build.cpp
	$(CC) $(CFLAGS) -c build.cpp -o $(BIN_FILE_PATH)build.o
	echo -n $(BIN_FILE_PATH) > $(BIN_FILE_PATH)build.d
	$(CC) $(CFLAGS) -MM build.cpp >> $(BIN_FILE_PATH)build.d
	$(CC) -o ./tmp.out $(BIN_FILE_PATH)build.o $(LDFLAGS)
	./tmp.out
	rm ./tmp.out

$(BIN_FILE_PATH)%.o: %.cpp
	$(CC) $(CFLAGS) -c $*.cpp -o $(BIN_FILE_PATH)$*.o
	echo -n $(BIN_FILE_PATH) > $(BIN_FILE_PATH)$*.d
	$(CC) $(CFLAGS) -MM $*.cpp >> $(BIN_FILE_PATH)$*.d



OBJ_WIN = $(CPP:%.cpp=$(BIN_FILE_PATH)%.win.o)

-include $(OBJ_WIN:%.o=%.d)

win: build $(OBJ_WIN)
	$(CC_WIN) -o $(BIN_FILE_PATH)$(NAME).exe $(OBJ_WIN) $(LDFLAGS) $(PLUS_LDFLAGS_WIN)

$(BIN_FILE_PATH)%.win.o: %.cpp
	$(CC_WIN) $(CFLAGS) -c $*.cpp -o $(BIN_FILE_PATH)$*.win.o
	echo -n $(BIN_FILE_PATH) > $(BIN_FILE_PATH)$*.win.d
	$(CC_WIN) $(CFLAGS) -MM $*.cpp >> $(BIN_FILE_PATH)$*.win.d

test: all
	$(BIN_FILE_PATH)$(NAME)

clean:
	rm $(BIN_FILE_PATH)*
