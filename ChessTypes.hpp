#pragma once

#include <string>
#include <tuple>
#include <cmath>
#include <limits>

namespace ChessTypes
{
  typedef int8_t Player;

  constexpr Player WHITE = 0;
  constexpr Player BLACK = 1;
  constexpr Player NO_PLAYER = 2;

  inline std::string getUnicodePlayer(const Player player)
  {
    if(player==WHITE)
    {
      return "White";
    }
    if(player==BLACK)
    {
      return "Black";
    }
    if(player==NO_PLAYER)
    {
      return "-";
    }
    return "?";
  }

  typedef int8_t Piece;

  constexpr Piece PAWN = 0;
  constexpr Piece KNIGHT = 1;
  constexpr Piece BISHOP = 2;
  constexpr Piece ROOK = 3;
  constexpr Piece QUEEN = 4;
  constexpr Piece KING = 5;
  constexpr Piece NO_PIECE = 6;

  inline std::string getUnicodePiece(const Player player, const Piece piece)
  {
    if(player == BLACK)
    {
      switch(piece)
      {
        case PAWN: return "\u2659";
        case KNIGHT: return "\u2658";
        case BISHOP: return "\u2657";
        case ROOK: return "\u2656";
        case QUEEN: return "\u2655";
        case KING: return "\u2654";
        case NO_PIECE: return "-";
        default: return "?";
      }
    }
    if(player == WHITE)
    {
      switch(piece)
      {
        case PAWN: return "\u265F";
        case KNIGHT: return "\u265E";
        case BISHOP: return "\u265D";
        case ROOK: return "\u265C";
        case QUEEN: return "\u265B";
        case KING: return "\u265A";
        case NO_PIECE: return "-";
        default: return "?";
      }
    }
    if(player == NO_PLAYER)
    {
      return "-";
    }
    return "?";
  }

  inline std::string getPieceNotation(const Player player, const Piece piece)
  {
    if(player == BLACK)
    {
      switch(piece)
      {
        case PAWN: return "p";
        case KNIGHT: return "n";
        case BISHOP: return "b";
        case ROOK: return "r";
        case QUEEN: return "q";
        case KING: return "k";
        case NO_PIECE: return "-";
        default: return "?";
      }
    }
    if(player == WHITE)
    {
      switch(piece)
      {
        case PAWN: return "P";
        case KNIGHT: return "N";
        case BISHOP: return "B";
        case ROOK: return "R";
        case QUEEN: return "Q";
        case KING: return "K";
        case NO_PIECE: return "-";
        default: return "?";
      }
    }
    if(player == NO_PLAYER)
    {
      return "-";
    }
    return "?";
  }

  inline std::tuple<Player, Piece> getPiece(const std::string s)
  {
    if(s=="p")
    {
      return std::tuple<Player, Piece>(BLACK, PAWN);
    }
    if(s=="n")
    {
      return std::tuple<Player, Piece>(BLACK, KNIGHT);
    }
    if(s=="b")
    {
      return std::tuple<Player, Piece>(BLACK, BISHOP);
    }
    if(s=="r")
    {
      return std::tuple<Player, Piece>(BLACK, ROOK);
    }
    if(s=="q")
    {
      return std::tuple<Player, Piece>(BLACK, QUEEN);
    }
    if(s=="k")
    {
      return std::tuple<Player, Piece>(BLACK, KING);
    }

    if(s=="P")
    {
      return std::tuple<Player, Piece>(WHITE, PAWN);
    }
    if(s=="N")
    {
      return std::tuple<Player, Piece>(WHITE, KNIGHT);
    }
    if(s=="B")
    {
      return std::tuple<Player, Piece>(WHITE, BISHOP);
    }
    if(s=="R")
    {
      return std::tuple<Player, Piece>(WHITE, ROOK);
    }
    if(s=="Q")
    {
      return std::tuple<Player, Piece>(WHITE, QUEEN);
    }
    if(s=="K")
    {
      return std::tuple<Player, Piece>(WHITE, KING);
    }
    return std::tuple<Player, Piece>(NO_PLAYER, NO_PIECE);
  }

  typedef int8_t Ply;
  const Ply MAX_DEPTH = INT8_MAX;

  typedef int16_t Value;
  constexpr Value VALUE_PAWN = 100;
  constexpr Value VALUE_KNIGHT = 320;
  constexpr Value VALUE_BISHOP = 330;
  constexpr Value VALUE_ROOK = 520;
  constexpr Value VALUE_QUEEN = 980;
  constexpr Value VALUE_NO_PIECE = 0;
  constexpr Value VALUE_OF_ALL_PIECES = (8*VALUE_PAWN + 2*VALUE_KNIGHT + 2*VALUE_BISHOP + 2*VALUE_ROOK + VALUE_QUEEN)*2;
  constexpr Value VALUE_KING = VALUE_OF_ALL_PIECES;
  constexpr Value VALUE_MATE = VALUE_OF_ALL_PIECES*2 + MAX_DEPTH;
  constexpr Value VALUE_DRAW = 0;
  constexpr Value VALUE[7] = {VALUE_PAWN, VALUE_KNIGHT, VALUE_BISHOP, VALUE_ROOK, VALUE_QUEEN, VALUE_KING, VALUE_NO_PIECE};
  constexpr Value VALUE_INFINITY = INT16_MAX - 1;
  constexpr Value NO_VALUE = INT16_MAX;

  typedef float Probability;
  inline Probability valueToProbability(Value value)
  {
    Probability ret = 1.0/(1.0+std::pow(10.0, -((float)value/(float)VALUE_PAWN)/4.0));
    return ret;
  }

  typedef uint8_t NodeType;
  constexpr NodeType PV_NODE = 0;
  constexpr NodeType ALL_NODE = 1;
  constexpr NodeType CUT_NODE = 2;
  constexpr NodeType NO_NODE = 2;

  typedef uint8_t Square;
  constexpr Square NO_SQUARE = 64;
  constexpr Square A1 =  0;
  constexpr Square B1 =  1;
  constexpr Square C1 =  2;
  constexpr Square D1 =  3;
  constexpr Square E1 =  4;
  constexpr Square F1 =  5;
  constexpr Square G1 =  6;
  constexpr Square H1 =  7;
  constexpr Square A2 =  8;
  constexpr Square B2 =  9;
  constexpr Square C2 = 10;
  constexpr Square D2 = 11;
  constexpr Square E2 = 12;
  constexpr Square F2 = 13;
  constexpr Square G2 = 14;
  constexpr Square H2 = 15;
  constexpr Square A3 = 16;
  constexpr Square B3 = 17;
  constexpr Square C3 = 18;
  constexpr Square D3 = 19;
  constexpr Square E3 = 20;
  constexpr Square F3 = 21;
  constexpr Square G3 = 22;
  constexpr Square H3 = 23;
  constexpr Square A4 = 24;
  constexpr Square B4 = 25;
  constexpr Square C4 = 26;
  constexpr Square D4 = 27;
  constexpr Square E4 = 28;
  constexpr Square F4 = 29;
  constexpr Square G4 = 30;
  constexpr Square H4 = 31;
  constexpr Square A5 = 32;
  constexpr Square B5 = 33;
  constexpr Square C5 = 34;
  constexpr Square D5 = 35;
  constexpr Square E5 = 36;
  constexpr Square F5 = 37;
  constexpr Square G5 = 38;
  constexpr Square H5 = 39;
  constexpr Square A6 = 40;
  constexpr Square B6 = 41;
  constexpr Square C6 = 42;
  constexpr Square D6 = 43;
  constexpr Square E6 = 44;
  constexpr Square F6 = 45;
  constexpr Square G6 = 46;
  constexpr Square H6 = 47;
  constexpr Square A7 = 48;
  constexpr Square B7 = 49;
  constexpr Square C7 = 50;
  constexpr Square D7 = 51;
  constexpr Square E7 = 52;
  constexpr Square F7 = 53;
  constexpr Square G7 = 54;
  constexpr Square H7 = 55;
  constexpr Square A8 = 56;
  constexpr Square B8 = 57;
  constexpr Square C8 = 58;
  constexpr Square D8 = 59;
  constexpr Square E8 = 60;
  constexpr Square F8 = 61;
  constexpr Square G8 = 62;
  constexpr Square H8 = 63;

  inline Square mirror(Square s)
  {
    return (7 - s/8)*8 + s%8;
  }

  inline std::string getSquareNotation(const Square i)
  {
    switch(i)
    {
      case A1: return "a1";
      case B1: return "b1";
      case C1: return "c1";
      case D1: return "d1";
      case E1: return "e1";
      case F1: return "f1";
      case G1: return "g1";
      case H1: return "h1";
      case A2: return "a2";
      case B2: return "b2";
      case C2: return "c2";
      case D2: return "d2";
      case E2: return "e2";
      case F2: return "f2";
      case G2: return "g2";
      case H2: return "h2";
      case A3: return "a3";
      case B3: return "b3";
      case C3: return "c3";
      case D3: return "d3";
      case E3: return "e3";
      case F3: return "f3";
      case G3: return "g3";
      case H3: return "h3";
      case A4: return "a4";
      case B4: return "b4";
      case C4: return "c4";
      case D4: return "d4";
      case E4: return "e4";
      case F4: return "f4";
      case G4: return "g4";
      case H4: return "h4";
      case A5: return "a5";
      case B5: return "b5";
      case C5: return "c5";
      case D5: return "d5";
      case E5: return "e5";
      case F5: return "f5";
      case G5: return "g5";
      case H5: return "h5";
      case A6: return "a6";
      case B6: return "b6";
      case C6: return "c6";
      case D6: return "d6";
      case E6: return "e6";
      case F6: return "f6";
      case G6: return "g6";
      case H6: return "h6";
      case A7: return "a7";
      case B7: return "b7";
      case C7: return "c7";
      case D7: return "d7";
      case E7: return "e7";
      case F7: return "f7";
      case G7: return "g7";
      case H7: return "h7";
      case A8: return "a8";
      case B8: return "b8";
      case C8: return "c8";
      case D8: return "d8";
      case E8: return "e8";
      case F8: return "f8";
      case G8: return "g8";
      case H8: return "h8";
      default: return "??";
    }
  }
  inline Square getSquareIndex(const std::string s)
  {
    if(s == "a1" || s == "A1"){ return A1; }
    if(s == "b1" || s == "B1"){ return B1; }
    if(s == "c1" || s == "C1"){ return C1; }
    if(s == "d1" || s == "D1"){ return D1; }
    if(s == "e1" || s == "E1"){ return E1; }
    if(s == "f1" || s == "F1"){ return F1; }
    if(s == "g1" || s == "G1"){ return G1; }
    if(s == "h1" || s == "H1"){ return H1; }
    if(s == "a2" || s == "A2"){ return A2; }
    if(s == "b2" || s == "B2"){ return B2; }
    if(s == "c2" || s == "C2"){ return C2; }
    if(s == "d2" || s == "D2"){ return D2; }
    if(s == "e2" || s == "E2"){ return E2; }
    if(s == "f2" || s == "F2"){ return F2; }
    if(s == "g2" || s == "G2"){ return G2; }
    if(s == "h2" || s == "H2"){ return H2; }
    if(s == "a3" || s == "A3"){ return A3; }
    if(s == "b3" || s == "B3"){ return B3; }
    if(s == "c3" || s == "C3"){ return C3; }
    if(s == "d3" || s == "D3"){ return D3; }
    if(s == "e3" || s == "E3"){ return E3; }
    if(s == "f3" || s == "F3"){ return F3; }
    if(s == "g3" || s == "G3"){ return G3; }
    if(s == "h3" || s == "H3"){ return H3; }
    if(s == "a4" || s == "A4"){ return A4; }
    if(s == "b4" || s == "B4"){ return B4; }
    if(s == "c4" || s == "C4"){ return C4; }
    if(s == "d4" || s == "D4"){ return D4; }
    if(s == "e4" || s == "E4"){ return E4; }
    if(s == "f4" || s == "F4"){ return F4; }
    if(s == "g4" || s == "G4"){ return G4; }
    if(s == "h4" || s == "H4"){ return H4; }
    if(s == "a5" || s == "A5"){ return A5; }
    if(s == "b5" || s == "B5"){ return B5; }
    if(s == "c5" || s == "C5"){ return C5; }
    if(s == "d5" || s == "D5"){ return D5; }
    if(s == "e5" || s == "E5"){ return E5; }
    if(s == "f5" || s == "F5"){ return F5; }
    if(s == "g5" || s == "G5"){ return G5; }
    if(s == "h5" || s == "H5"){ return H5; }
    if(s == "a6" || s == "A6"){ return A6; }
    if(s == "b6" || s == "B6"){ return B6; }
    if(s == "c6" || s == "C6"){ return C6; }
    if(s == "d6" || s == "D6"){ return D6; }
    if(s == "e6" || s == "E6"){ return E6; }
    if(s == "f6" || s == "F6"){ return F6; }
    if(s == "g6" || s == "G6"){ return G6; }
    if(s == "h6" || s == "H6"){ return H6; }
    if(s == "a7" || s == "A7"){ return A7; }
    if(s == "b7" || s == "B7"){ return B7; }
    if(s == "c7" || s == "C7"){ return C7; }
    if(s == "d7" || s == "D7"){ return D7; }
    if(s == "e7" || s == "E7"){ return E7; }
    if(s == "f7" || s == "F7"){ return F7; }
    if(s == "g7" || s == "G7"){ return G7; }
    if(s == "h7" || s == "H7"){ return H7; }
    if(s == "a8" || s == "A8"){ return A8; }
    if(s == "b8" || s == "B8"){ return B8; }
    if(s == "c8" || s == "C8"){ return C8; }
    if(s == "d8" || s == "D8"){ return D8; }
    if(s == "e8" || s == "E8"){ return E8; }
    if(s == "f8" || s == "F8"){ return F8; }
    if(s == "g8" || s == "G8"){ return G8; }
    if(s == "h8" || s == "H8"){ return H8; }
    return NO_SQUARE;
  }
}
