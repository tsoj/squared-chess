#pragma once

#include "TranspositionTable.hpp"
#include "MoveList.hpp"

#include <chrono>
#include <atomic>

Move startSearch(
  TranspositionTable& tt,
  std::vector<Position> history,
  Position position,
  Ply maxDepth,
  int msLeft,
  int msIncPerMove,
  int movesToGo,
  std::atomic<bool>& shouldStop
);
